\subsection{Data Flow}
\label{sect:solutions:dataflow}

A DSL for processes that communicate concurrently through channels.

\subsubsection{Motivation}

Embedded Systems usually have different tasks that are operated by either the
main loop, or the RTOS scheduler. Communication between tasks is often very
tricky, either through global variables, message queues or mailboxes.

Race conditions, described at~\ref{sect:bugs:race-conditions}, are a source
of errors. The problem with race conditions is that they only happen so often,
under very specific conditions, which makes it difficult to reproduce during
testing.

\subsubsection{Proposal}

The proposed solution is to embed a dataflow language. Under the dataflow
paradigm, algorithms are described as directed graphs where the nodes represent
computations and the arcs represent the data paths \cite{lee1987synchronous}.

The principle is that any node can fire (execute), if input values are
available at the input arcs.

\textsc{Matlab} Simulink and LabView are two popular graphical dataflow
programs. Those are graphical programs that are developed outside the source
code of the actual embedded system. Using a textual equivalent brings this to
plain text code. In Simulink and LabView each computational element is a
separate node. When using plain C inside the nodes, anything can be done inside
such a node, so the nodes will describe the dataflow on a slightly higher,
coordination level.

\begin{listing}[H]
  \begin{minted}{c}
// interrupt reading a value from the first sensor
isr sensor0 returns (uint32 s0) {
  s0 = peripherals[PERIPHERALS_S0];
}
// reading from another sensor
isr sensor1 returns (uint32 s1) {
  s1 = peripherals[PERIPHERALS_S1];
}
// calculate the min and max of the two inputs
task minmax(sensor0.s0, sensor1.s1 => sensor1.b)
    returns (uint32 min, uint32 max) {
  min = s0 < b ? s0 : b;
  max = s0 > b ? s0 : b;
}
  \end{minted}
  \caption{Example for calculating the minimum and maximum the values of two sensors}
  \label{lst:solutions:dataflow-minmax}
\end{listing}

The code in~\ref{lst:solutions:dataflow-minmax} shows a very simple example
where two interrupts are defined that provide data, which is used by a tasks
that calculates the minimum and maximum values and passes those on to the
output. Graphically the code corresponds with the diagram in
figure~\ref{fig:solutions:dataflow-minmax}.

\begin{figure}[H]
  \centering
  \includegraphics{solutions/assets/dataflow-minmax.pdf}
  \caption{A graphical representation of the `minmax' dataflow program}
  \label{fig:solutions:dataflow-minmax}
\end{figure}

The two interrupts that read some value from some memory location are the two
sources of the diagram. To indicate that these are interrupt service routines,
they are defined with the \texttt{isr} keyword, and such a node can only
provide values.

A task, which is an RTOS task, is defined by the \texttt{task} keyword. A task
can reference to one or more input channels, from other \texttt{isr} or
\texttt{task} nodes. The task can define zero, one or more output channels.  As
the \texttt{returns} output channels are typed, the inputs do not necessarily
to be typed.

After the execution of an \texttt{isr} or \texttt{task}, the values assigned to
an output are pushed to the output channel. In the case of \texttt{isr sensor0}
in example~\ref{lst:solutions:dataflow-minmax} this is variable \texttt{s0}.
The value is written to the buffer of channel \texttt{sensor0.s0}.
See~\ref{sect:solutions:dataflow:buffers} how buffers work.

An input rename is used in example~\ref{lst:solutions:dataflow-minmax}: the
\texttt{sensor1.s1} channel is renamed to \texttt{sensor1.b} channel using the
``\texttt{=>}'' operator. If a channel is renamed like \texttt{a.b~=>~.x} the
name of the channel is not essential, only the local variable ``\texttt{x}'' is
used for the value.

\paragraph{Previous values}

For applications like control-loops or data filters, we would like to connect
the outputs back to the inputs. Doing this directly would create circular
dependencies and nondeterminism: the output of the node cannot be computed
because the input depends on the not computed output.

The ``\texttt{->}'' operator, the followed-by operator, prepends a value
at the beginning of the stream of values of a channel.

\begin{equation}
  \begin{aligned}
    X = \{& x_0, x_1, x_2, x_3, \dots\} \\
    y \rightarrow X = \{& y, x_0, x_1, x_2, \ldots\}
  \end{aligned}
  \label{eq:solutions:dataflow:delay}
\end{equation}

Equation \eqref{eq:solutions:dataflow:delay} shows how the followed-by operator
works, instead of $x_0$ as the first value, the first value is $y$, which is
then followed by the values of $X$, $x_0, x_1, \dots$. The followed-by operator
could be nested, like $y_0 \rightarrow y_1 \rightarrow X$ to achieve a delay of
two: $y_0 \rightarrow y_1 \rightarrow X = \{y_0, y_1, x_0, x_1, \dots\}$.


An example of how the followed-by operator can be used is shown in code
example~\ref{lst:solutions:dataflow-delay}.  The first input of the
\texttt{counter} task reads of the \texttt{tick.t} channel. The second input
uses the followed-by operator. As the followed-by operator creates a new
unnamed channel, we have to rename the new channel. As we are only interested
in the value, so we can rename it to just \texttt{.count}. We increment this
\texttt{count} value with the value from \texttt{tick.t}. As the output channel
is also named \texttt{count}, the incremented \texttt{count} value is directly
the output value of the \texttt{counter} task.

\begin{listing}[H]
  \begin{minted}{c}
// some (clock) tick interrupt. Pushes '1's to the output channel
isr tick returns (uint32 t) {
  t = 1;
}
// create a task which adds the previous count with the tick value
task counter(tick.t,
             0 -> counter.count => .count)
    returns (uint32 count) {
  count += t;
}
  \end{minted}
  \caption{Example of getting the previous value of the channel}
  \label{lst:solutions:dataflow-delay}
\end{listing}

\paragraph{Merge Operation}

A task can only do its computation once all inputs are available. There are
applications where a task only needs to read from either of the inputs to
continue, for example a logger that reads from multiple sources. The merge
operation can be used for that.

To merge two channels, the type of the values of the channels should be
compatible.

\begin{listing}[H]
  \begin{minted}{c}
// suppose X and Y are some other tasks
// with outputs a and b respectively.
task merger(X.a | Y.b => A.c) returns (uint32 d) {
  // The value of the merger is bound to "c"
  // assign it to the output
  d = c;
  // to know from which input the value came from
  // the channels can be compared
  if (A.c == X.a) {
    // do thing
  } else {
    // do other thing
  }
}
  \end{minted}
  \caption{Example of a task merging two inputs}
  \label{lst:solutions:dataflow-merger}
\end{listing}

Code example~\ref{lst:solutions:dataflow-merger} shows an example how two input
channels are merged. The binary ``\texttt{|}'' operator is used to merge the
two inputs, and the resulting channel is bound to \texttt{A.c}, so the value
to the \texttt{c} variable because of the ``\texttt{=>}'' operator.

The merge operator takes two channels and returns a new one, so by nesting the
merge, merging more than two channels will become possible, for example
$X | Y | Z \Rightarrow C$, merges $X$, $Y$ and $Z$ and binds the result to $C$.

\vspace{10mm}
\todo[inline,size=\scriptsize]{As I understand correctly, this operation is not fully
  deterministic. I think, using dynamic scheduling, as compiling it to RTOS
  tasks, semaphores etc.\ it is perfectly possible to implement. I am not sure
  yet if this version is perfect when you want to take advantage of synchronous
  dataflow (SDF) analysis/verification/optimizations.}

\paragraph{Embedded State Machine Task}

A task and the State Machine from section~\ref{sect:solutions:statemachine} can
be combined to a state machine task.

Figure~\ref{fig:solutions:dataflow-statemachine} shows an diagram of a
state machine, with the states and transitions. This state machine could be
encoded as shown in code example~\ref{lst:solutions:dataflow-statemachine}.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{solutions/assets/dataflow-statemachine.pdf}
  \caption{A diagram of a turnstile state machine}
  \label{fig:solutions:dataflow-statemachine}
\end{figure}


\begin{listing}[H]
  \begin{minted}{c}
enum turnstile_action {lock, unlock, none};
// a 'fsmtask' rather than a 'task' definition
fsmtask turnstile(X.coin | Y.push)
    // the initial state is 'locked'
    returns(enum turnstile_action action) = locked {
  state locked {
    event coin do { action = unlock; } then unlocked;
    event push do { action = none;   } then locked;
  }
  state unlocked {
    event coin do { action = none;   } then unlocked;
    event push do { action = lock;   } then locked;
  }
}
// Another task can read the state of the fsmtask as input.
task foo(turnstile.state, turnstile.action) {
  // do something
}
  \end{minted}
  \caption{Example of a task merging two inputs}
  \label{lst:solutions:dataflow-statemachine}
\end{listing}

This state machine task has a slightly different definition than a normal task,
so is defined by the \texttt{fsmtask} keyword. For a \texttt{fsmtask} the
transitions are dependent on the inputs of the task. In case of a merged input,
using the ``$|$'' operator, the transitions will correspond with the matched
input. Optionally transitions can have guards, just like in the normal state
machine, where the values of the inputs can be used.

The \texttt{fsmtask} maps the previous state and the inputs to a new state and
optional side-outputs. This new state and outputs can be used by other tasks.
The state of the state machine is automatically mapped to the \texttt{state}
output, while other outputs can be chosen freely.

\paragraph{Error Handling}

Errors can occur when something unexpected happens in the system. For example
in a node that divides \texttt{a} by \texttt{b}, b can be $0$ which would cause
an error. To handle this case, the error handling explained in
section~\ref{sect:solutions:errors} works for the dataflow tasks too.
Example~\ref{lst:solutions:dataflow-errors} shows how the output variable can
be of the \texttt{MaybeError} type. The consumer of the channel is then forced
to handle this case.

\begin{listing}[H]
  \begin{minted}{c}
task safe_divider(x.a, y.b) returns (MaybeError<int32> c) {
  if (b == 0) {
    c = None();
  } else {
    c = a / b;
  }
}
task bar(safe_divider.c) {
  try {
    // assignments in the try-block are a special case if the right
    // hand side is a MaybeError instance. In that case the assignment
    // is ok, or there is a jump to the catch block
    int32 d = c;
  } catch {
    // oops.
  }
}
  \end{minted}
  \caption{Example to handle errors}
  \label{lst:solutions:dataflow-errors}
\end{listing}



\paragraph{Output Selector}

A normal dataflow node maps input values to output values. This is analogous to
the functional \texttt{map} operator. When it is only desired to assign an
output in a specific case, for example to filter specific values or only pass
values at a certain rate. As shown in code
example~\ref{lst:solutions:dataflow-select}, the output type can be of the
\texttt{When} type. When the value of the output variable is \texttt{Nothing},
the value is ignored and not written to the channel. When a normal value is
written to this variable, everything proceeds normally.

\begin{listing}[H]
  \begin{minted}{c}
task foo(bar.x) returns (When<int32> even, When<int32> odd) {
  if (x % 2 == 0) {
    even = x;
    odd = Nothing();
  } else {
    even = Nothing();
    odd = x;
  }
}
  \end{minted}
  \caption{Example how to filter a stream of values}
  \label{lst:solutions:dataflow-select}
\end{listing}

\paragraph{Task Hierarchy} \todo{Find a nice syntax}

To keep a clear overview of tasks, tasks can be ordered hierarchically into
parent and subtasks. This will help the developer to get a quicker overview
of the system. When generating visual diagrams the hierarchy will help ordering
the tasks and flows clearly.

\paragraph{Cloning values on the channels}

An output of a task can be connected to two or more other task inputs. In this
case the connect tasks all get the value from the source task. One important
reason to use the dataflow paradigm is to prevent shared variables. However if
a value is a pointer given to all the tasks, they still have access to the same
object, which each can modify, and keeping the original problem. To solve this
problem, the values are copied at the input side, so each task gets a different
value and cannot interfere with other tasks.

\paragraph{Scheduling and Buffer sizes}
\label{sect:solutions:dataflow:buffers}

Partitions of the dataflow graphs can satisfy \emph{Synchronous Data Flow}
(SDF) constraints~\cite{lee1987synchronous}. SDF graphs can be scheduled
statically, which means it is not necessary to create a separate operating
system task for each task and finite size channel buffers.

\hfill
\todo[inline,size=\scriptsize]{This needs further work on how it should be
  implemented\dots}


\subsubsection{Alternatives}

Another model is used by Hume~\cite{hammond2003hume}, where boxes are connected
with incoming and outgoing wires. Another possibility, that is related is
Communicating Sequential Processes (CSP), which is a formal language for
describing interaction between concurrent systems.
