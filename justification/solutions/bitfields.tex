\subsection{Bit Fields}
\label{sect:solutions:bitfields}

Bit manipulations are often used in C programming, and especially in embedded
software. This includes reading or writing bits from memory. This memory is
often connected to some peripheral device. Bit fields can be used for efficient
storage of a data structure with lots of flags.

Looking up bits, and setting specific bits involves creating bit masks,
shifting, OR-ing or AND-ing. An example is shown
in code example~\ref{lst:solutions:bitfields-flags}. Most C programmers will
understand this quite easily, but unfortunately it can be error-prone mixing up
\texttt{\&} and \texttt{\&\&}, omitting the \texttt{\~} when using \texttt{\&=}
to set bits.

\begin{listing}[H]
  \begin{minted}{c}
    // create a mask for a specific bit position
    #define FLAG_A (1 << 5)

    unsigned int flags;

    // set a bit
    flags |= FLAG_A;

    // unset a bit
    flags &= ~FLAG_A;

    // toggle a bit
    flags ^= FLAG_A;

    // test a bit
    if (flags & FLAG_A) ...
  \end{minted}
  \caption{Typical bit operations in a C program}
  \label{lst:solutions:bitfields-flags}
\end{listing}

A solution would be to use bit fields of C structures. The \texttt{\#define}s
that represent the flags can be replaced by the definition of fields.
Bit fields specify the number of bits to use. Accessing bits works as shown in
example~\ref{lst:solutions:bitfields-c-examples}:

\begin{listing}[H]
  \begin{minted}{c}
    typedef struct {
      unsigned int is_flag_a : 1;
      unsigned int is_flag_b : 1;
    } flags;
  \end{minted}
  \caption{C Bit-fields}
  \label{lst:solutions:bitfields-c-examples}
\end{listing}

Using bit fields, as in this example, fields can be accessed by referencing the
names of the fields, like any structure members: \texttt{flags.is\_flag\_a}
or \texttt{flags.is\_flag\_b}.

A trick to use this bit field notation for other memory locations is to assign
a pointer of the struct to the beginning of the memory, as shown
in example~\ref{lst:solutions:bitfields-pointer-struct}.

% more information about this:
% http://www.pagetable.com/?p=250
% http://jcardente.blogspot.nl/2009/10/c-bitfields-and-hw-registers.html

\begin{listing}[H]
  \begin{minted}{c}
#include <stdio.h>

// Some variable
unsigned int A = 0xE0110000;

// Define the structure of the memory
typedef struct {
  unsigned int f1 : 8;
  unsigned int f2 : 8;
  unsigned int f3 : 16;
} register_A;

int main() {

  // Assign the variable pointer to the memory address
  register_A * barA = (register_A *) &A;
  printf("%04x%02x%02x %08x\n", barA->f3, barA->f2, barA->f1, A);

  barA->f1 = 0xD0;
  printf("%04x%02x%02x %08x\n", barA->f3, barA->f2, barA->f1, A);

  barA->f2 = 0xCF;
  printf("%04x%02x%02x %08x\n", barA->f3, barA->f2, barA->f1, A);

  return 0;
}

// Using GCC 4.8 on Ubuntu this would result in
// e0110000 e0110000
// e01100d0 e01100d0
// e011cfd0 e011cfd0
  \end{minted}
  \caption{C Bit-fields}
  \label{lst:solutions:bitfields-pointer-struct}
\end{listing}

One problem with struct bit fields is that the C specification does not specify
the exact implementation and alignment of the bits. The compiler is free to
implement this. That makes code
like~\ref{lst:solutions:bitfields-pointer-struct} unportable to different
platforms. Some compilers might also generate more verbose code compared to
bitwise operations. Finally only \texttt{unsigned int} and \texttt{int} are
supposed to be the datatypes of the bitfield members, some compilers however
also accept \texttt{char} as an extension~\cite{mann2004program}.

\subsubsection{Solution}

What we would like to have is a method that is as readable as defining
bit fields in structs and where it is not necessary to fiddle with bits too
much to simply read or write values.

To do so, the structure of bit fields could be defined, as how the hardware may
map the bits to memory. Then the usage would look like usual structures, where
field expressions can be used to set or read values.

\begin{listing}[H]
  \begin{minted}{c}
// suppose this 0xFA is some memory location
unsigned int * HARDWARE_VALUE = (unsigned int *) 0xFA;
// define the stucture of the data.
bitfields HardwareSettings {
  flag_a : 1; // flag_a is at the most right bit
  flag_b : 1; // flag_b is at the second bit
  setting_x : 4; // setting_x consists of 4 bits, bits 3, 4, 5 and 6.
};

int32 main() {

  // some value without initializer
  HardwareSettings x;
  x.flag_a = 1;
  x.flag_b = !x.flag_a;

  // value with initializer.
  HardwareSettings *hs = HARDWARE_VALUE;

  // set the flag
  (*hs).flag_a = 1;
  // unset the flag
  (*hs).flag_a = 0;
  // test the flag
  if ((*hs).flag_a) {
    // ...
  }

  // check for the bit size
  (*hs).flag_b = 4; // warning: value 4 requires 3 bits

  return 0;
}
  \end{minted}
  \caption{C Bit-fields}
  \label{lst:solutions:bitdata}
\end{listing}



