\subsection{Error Handling}
\label{sect:solutions:errors}

To signal errors, C functions often return a special value, for example
\texttt{-1} or \texttt{null} are used. The consumer of the function must
explicitly check for these values. Bugs can occur if that is forgotten, as is
described at~\ref{sect:bugs:error-handling}. This can be solved in two parts:
(1) using a \texttt{Maybe} or \texttt{Option} type that are present in Haskell
or Scala. To get the value from the wrapper, the programmer has to extract the
value at which point also the error case must be handled, and (2) a try-catch
or pattern match syntax at the consumer side to conveniently handle errors.

Example~\ref{lst:solutions:errors-divide} shows a safe division function that
handles the case when the dividend equals $0$ gracefully. The
\texttt{safe\_divide} returns a \texttt{MaybeError} type that can hold a
\texttt{int32} value. The function returns either an value, or an
\texttt{Error()}, which signals an error. In the \texttt{main} function, a
attempt-fail block unpacks the \texttt{MaybeError} value

\begin{listing}[H]
  \begin{minted}{c}
// A function that might fail
MaybeError<int32> safe_divide(int32 a, int32 b) {
  return (b == 0) return Error();
  else return (a / b);
}

int main() {
  // A function that returns a MaybeError must be
  // used inside a fail-attempt so errors are forced to
  // be handled.
  attempt {
    // this will work fine
    int32 c ?= safe_divide(4, 2);
    // this will jump to the catch block
    int32 d ?= safe_divide(4, 0);
  } fail {
    printf("an error occurred\n");
  }
  return 0;
}
  \end{minted}
  \caption{A function that returns a MaybeError value and its usage.}
  \label{lst:solutions:errors-divide}
\end{listing}

\begin{framed}
  \textbf{Note} that currently variable declarations that `unpack' the error
  use a \texttt{?=} notation. Technically this is not strictly necessary, but
  I think it conveys the meaning of the declaration better than simply the
  \texttt{=} symbol, as it can be mixed with other variable declarations that
  do not call functions that can return an error.
\end{framed}

The \texttt{MaybeError} type can have one or two values, it contains either the
value, or the value and some error object. This can be anything, for example an
integer error code or an enumeration constant. An example of this is shown in
example~\ref{lst:solutions:errors-stack}. This example shows a pop and a push
function on a stack of values. The pop function can fail if there are no values
on the stack while the push function can fail if the stack is full.

\begin{listing}[H]
  \begin{minted}[fontsize=\footnotesize]{c}
// For example with a fixed size stack, there are only
// 10 available spaces.
const int32 SIZE = 10
int32 stack[SIZE];
// pointer contains the next write position
int32 sptr = 0;
// define errors
typedef enum {empty} empty_error;
typedef enum {full} full_error;
// add a value to the stack.
// this MaybeError<void> is a parameterized type. C doesn't know this
// kind of types at all, so this needs some hard-coding or bike-shedding.
MaybeError<void,stack_error> push(int32 x) {
  if (sptr < SIZE) {
    stack[sptr++] = x; // success, return void
  } else {
    // no space anymore, signal the caller by returning Error()
    return Error(full);
  }
}
// get the latest value, if one exists
MaybeError<int32,stack_error> pop() {
  if (sptr > 0) {
    return stack[--sptr];
  } else {
    return Error(empty);
  }
}

// consumer end
int main() {
  // using fail-attempt here. Using the Maybe type, it's a bit like
  // the >>= (bind) operator or do notation in Haskell for Monads.
  // fail-attempt might be a bit confusing for C++/Java/Rest-of-the-world.
  attempt {
    void p1 ?= push(1);
    void p2 ?= push(2);
    int32 last1 ?= pop(); // unpack the Maybe
    printf("%d\n", last1); // and use directly
    int32 last2 ?= pop();
    int32 last3 ?= pop(); // oops, Error returned, go to the fail of empty
  } fail (empty_error e){
    printf("The stack is empty\n");
  } fail (full_error e){
    printf("The stack is full\n");
  }
  return 0;
}
  \end{minted}
  \caption{Example of an optional error code}
  \label{lst:solutions:errors-stack}
\end{listing}

\subsubsection{Generated C output}

The generated code for the following example

\begin{listing}[H]
  \begin{minted}{c}
MaybeError<int32,uint8> f(uint8 a) {
  if (a == 0) return Error(1 + 2);
  return a;
}
void h() {
  attempt[a] {
    uint8 x ?= f(1);
  } fail (uint8 a) {
    a;
  }
}
  \end{minted}
  \caption{Minimal example with a error type}
  \label{lst:solutions:errors-generating-c}
\end{listing}

Looks like the following:

\begin{listing}[H]
  \begin{minted}{c}
typedef struct  {
  unsigned char success;
  union  {
    signed int value;
    unsigned char error;
  } result;
} __MaybeError_f;

unsigned char f (__MaybeError_f *__maybeErrorReturn_f, unsigned char a) {
  if (a == 0) return (
    __maybeErrorReturn_f->result.error = 1 + 2,
    __maybeErrorReturn_f->success = 0
  );
  return (
    __maybeErrorReturn_f->result.value = a,
    __maybeErrorReturn_f->success = 1
  );
}

void h () {
  unsigned char attempt_a_1_a;
  {
    __MaybeError_f __maybe_x;
    f(&__maybe_x, 1);
    if (!__maybe_x.success) {
      attempt_a_1_a = __maybe_x.result.error;
      goto attempt_a_fail_1;
    }
    unsigned char x = __maybe_x.result.value;
    goto attempt_a_finally;
  }
  attempt_a_fail_1: {
    {
      attempt_a_1_a;
    }
    goto attempt_a_finally;
  }
  attempt_a_finally: {
  }
}
  \end{minted}
  \caption{The generated C code of the previous example}
  \label{lst:solutions:errors-generated-c}
\end{listing}

The basic principle is that it uses a \texttt{struct} with the success or
error values. That struct is passed into the called function. The
\texttt{attempt} and \texttt{fail} blocks are transformed into labels and
\texttt{goto}s. The analysis of the error type decides at compile-time which
\texttt{fail}-label should be executed might the function fail.

There is some room for improvement, described at
\url{https://github.com/arian/metac/labels/error}
