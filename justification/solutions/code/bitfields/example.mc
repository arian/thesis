
// define the structure of bitfields.
// only unsigned base-types are allowed. The defines a wildcard/padding

bitfields ToBitFields {
  a: 1;
  b: 1;
};

bitfields A {BitFields; c: 4; 6};

A a;
// No initial address, so it's safe to just create a local variable
// that is big enough for the A bitifields to fit in.
// > unsigned short a;

// link the variable to some pointer:
unsigned int _b;
A b = &(_b);
// > unsigned int *a = &(_a);
// type is resolved from the expression

// set bit at position 0 to 1
a.a = 1;
// > a |= (1 << 0);
// unset bit at position 2
a.b = 0;
// > a &= ~(1 << 1)

// set a range of bits
a.c = 8;
// > a = (a & (~(0xF << 2))) | (8 << 2);
// - clear old values with a mask
// - OR it with the new values
// - save it

printf("%d\n", a.c);
// > printf("%d\n", (a >> 2) & 0xF);

// toggle bit
a.a = !a.a;

// when the variable is using to some memory address (pointer)

b.a = 1;
// > (*a) |= 1;

// define a bitdata representation

bitset uint8 {1} bool;

// translates to
// > typedef unsigned char bool;

bitfield uint8 {r:bool; w:bool; x:bool;} permissions;
// translatates that fit the size of 3 times bool
// > typedef unsigned char persissions

permissions p = {.r = 1, .w = 1; .x = 1};
permissions p = {1,0,0};
// usage of the permissions type
// > permissions p = 0b100;

bitdata<22, 6, 1, permissions> Fpage;
// total size is 32, so tranlates to
// > typedef unsigned int Fpage;

