// Reading from either channel can be done using the
// 'alts' statement. The 'alts' statement contains
// communicating cases, which  channel reference
// or a read expression
process counter(chan<int> up, chan<int> down) {
  int counter, v;
  while (1) {
    alts {
      case [counter < 10] up: counter++;
      case down ? v: counter -= v;
    }
    printf("counter: %d\n", counter);
  }
}
