// Running this program would result in printing "31" once.
int main() {
  chan<int> c;
  par {
    // a write process that writes some value to the channel
    c ! 31;
    // A parallel anonymous process
    // which contains sequential statements
    {
      int y;
      c ? y;
      printf("%d\n", y);
    }
  }
  return 0;
}
