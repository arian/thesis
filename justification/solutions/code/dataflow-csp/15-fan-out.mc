process fanout(chan<int> c, chan<int> *outs) {
  while (1) {
    int x, i = 0;
    c ? x;
    while (1) {
      chan<int> out = outs[i++];
      if (out == NULL) {
        break;
      }
      out ! x;
    }
  }
}
