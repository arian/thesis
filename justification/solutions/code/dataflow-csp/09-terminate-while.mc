#include <stdio.h>

// This process will receive characters, until it should be stopped

process print_char(chan<char> c_chan, chan<int> stop) {
  int running = 1;
  char c;
  while (running) {
    alts {
      case c_chan ? c:
        putchar(c);
      case stop: {
        putchar('\n');
        running = 0;
      }
    }
  }
}

process send_chars(chan<char> c_chan, chan<int> stop, char * str) {
  int i = 0;
  do {
    c = str[i++];
    c_chan ! c;
  } while (c != '\0');
  stop ! 1;
}

int main() {
  chan<char> chars;
  chan<int> stop;

  char str[] = "how are you today?";

  par {
    send_chars(chars, stop, str);
    print_char(chars, stop);
  }
  return 0;
}
