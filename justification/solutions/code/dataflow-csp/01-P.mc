// A producer, that creates an infinite
// stream of increasing numbers
process P(chan<int> out) {
  int i = 0;
  while (1) {
    out ! i++;
  }
}
