// The Identity process
process ID(chan<int> in, chan<int> out) {
  int x;
  // using a while loop, so it will run continuously
  while (1) {
    in ? x;
    out ! x;
  }
}
