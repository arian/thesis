#include <stdio.h>

struct MyData { int32 x; };

process P(chan<struct MyData> c1, chan<struct MyData*> c2, chan<int> sync) {
  struct MyData d1, d2;
  int tmp;
  d1.x = 1;
  d2.x = 1;
  printf("P1: %d %d\n", d1.x, d2.x); // prints P1: 1 1
  c1 ! d1;
  c2 ! &d2;
  sync ? tmp;
  printf("P2: %d %d\n", d1.x, d2.x); // prints P2: 1 2
}

process C(chan<struct MyData> c1, chan<struct MyData*> c2, chan<int> sync) {
  struct MyData d1;
  struct MyData *d2;
  c1 ? d1;
  c2 ? d2;
  printf("C1: %d %d\n", d1.x, d2->x); // prints C1: 1 1
  d1.x = 2;
  d2->x = 2;
  sync ! 1;
  printf("C2: %d %d\n", d1.x, d2->x); // prints C2: 2 2
}

int main() {
  chan<struct MyData> c1;
  chan<struct MyData*> c2;
  chan<int> sync;
  par {
    C(c1, c2, sync);
    P(c1, c2, sync);
  }
  return 0;
}
