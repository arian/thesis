process P(chan<int> c) {
  int i;
  for (i = 0; i < 25; i++) {
    c ! i;
    if ((i+1) % 5 == 0) usleep(10e6);
  }
}
process C(chan<int> c, chan<int> close) {
  while (1) {
    int my_local_buffer[5], i, x;
    for (i = 0; i < 5; i++) {
      alts {
        case c ? x: my_local_buffer[i] = x;
        default:
          // can't read from channel anymore,
          // so it must be empty, so stop this for-loop
          break;
      }
    }
    // consume my_local_buffer...
  }
}
int main() {
  buffered[10] chan<int> c;
  chan<int> close;
  par {
    P(c);
    C(c);
  }
  return 0;
}
