#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

process do_timeout(chan<int> timedout, int ms) {
  usleep(rand() % ms);
  timedout ! 1;
}

process print_result(chan<int> timedout, chan<int> done) {
  int result;
  // these extra _done variables and while loop are necessary
  // to read from all channels, so the other parallel processes
  // do not block, as par { ... } joins all processes into the
  // main thread, so if one process would block, the program would
  // not terminate. This needs a more elegant solution
  uint8 timedout_done = 0, done_done = 0;
  while ((timedout_done + done_done) < 2) {
    // either of the alternative channels is selected
    alts {
      case timedout: {
        if (timedout_done + done_done == 0) {
          printf("too slow\n");
        }
        timedout_done = 1;
      }
      case done ? result: {
        if (timedout_done + done_done == 0) {
          printf("result is %d\n", result);
        }
        done_done = 1;
      }
    }
  }
  printf("done\n");
}

int main() {
  chan<int> timedout;
  chan<int> result;
  // this spawns and joins all processes
  par {
    do_timeout(timedout, 500);
    do_timeout(result, 500);
    print_result(timedout, result);
  }
  return 0;
}
