process fanin4(chan<int> out, chan<int> a, chan<int> b, chan<int> c, chan<int> d) {
  while (1) {
    int x;
    alts {
      case a ? x: out ! x;
      case b ? x: out ! x;
      case c ? x: out ! x;
      case d ? x: out ! x;
    }
  }
}
