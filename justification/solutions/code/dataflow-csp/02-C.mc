// A consumer
// it prints all values from an input channel
process C(chan<int> in) {
  int x;
  while (1) {
    in ? x;
    printf("%d\n", x);
  }
}
