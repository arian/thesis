
process P(chan<int> c | int y) {
  int x;

  c!x;

  if (x == 3) {
    P(4);
  } else {
    p(5);
  }
}

process C(chan<int> c, chan<int> d | int y) {
  int x;

  alts {
    [y == 0] c?x: {
      printf("c\n");
    }
    [y != 0] d?x: {
      printf("d\n");
    }
  }

  if (x) {
    C(1);
  } else {
    C(0);
  }
}
