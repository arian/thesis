#include <stdio.h>
// contains interrupts_{init,set,close} and peripherals
#include <interrupts.h>
#define MOUSE_BUFFER_SIZE 10
#define KEY_CODE_ENTER 28
typedef struct mouse_diff { int x; int y; } MouseDiff;
// define these two channels as buffered, which means non blocking.
// enters can contain one value, new values will be dropped
buffered chan<int> enters;
// this can contain 10 values. The 11th value will be dropped.
buffered[MOUSE_BUFFER_SIZE] chan<MouseDiff> mouse;
void keyboard_isr() {
  if (peripherals.keyboard_code == KEY_CODE_ENTER) {
    enters ! peripherals.keyboard_code;
  }
}
void mouse_isr() {
  MouseDiff md; md.x = peripherals.mouse_x; md.y = peripherals.mouse_y;
  mouse ! md;
}
process select_x_or_y(chan<int> keyboard_enters, chan<MouseDiff> mouse,
                      chan<int> out @ int direction) {
  MouseDiff md;
  alts { // change the state upon enter presses
    case keyboard_enters: select_x_or_y(!direction);
    case mouse?md: {
      // depending on the state, select a value from the mouse data
      if (direction == 1) { out ! md.x; }
      else { out ! md.y; }
      select_x_or_y(direction);
    }
  }
}
process print_value(chan<int> values) {
  printf("%d\n", values?); print_value();
}
int main() {
  // Initialize interrupts
  if (interrupts_init() != 0) { return 1; }
  // Register the ISR handlers
  interrupts_set(KEYBOARD_ISR, &keyboard_isr);
  interrupts_set(MOUSE_ISR, &mouse_isr);
  // channel with either x or y values from the mouse
  chan<int> values;
  par { select_x_or_y(enters, mouse, values @ 0); print_value(values); }
  // should never be reached as the processes run forever cleanup things
  if (interrupts_close() != 0) { return 1; }
  return 0;
}
