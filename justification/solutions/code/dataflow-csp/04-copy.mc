process copy() {
  chan<int> west;
  chan<int> east;
  // A 'par' block. Processes are constructed in parallel
  par {
    P(west); // Producer puts values on 'west'
    ID(west, east); // ID passes the value from west to east
    C(east); // Consumer consumes the value
  }
}
