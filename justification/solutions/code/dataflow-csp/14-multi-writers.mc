process print(char *label, chan<char> c) {
  while (1) printf("%s %c\n", label, c?);
}
process writer(char label, int delay, chan<char> c) {
  while (1) {
    c ! label;
    printf("%c\n", label);
    usleep(delay);
  }
}
int main() {
  chan<char> c;
  par {
    print("P", c);
    writer('A', 10e4, c);
    writer('B', 10e4, c);
    writer('C', 10e4, c);
  }
  return 0;
}
