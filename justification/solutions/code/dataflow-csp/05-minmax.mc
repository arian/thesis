// Reading from two channels.
// Also writing to two channels
process minmax(chan<int> s1, chan<int> s2, chan<int> min, chan<int> max) {
  int a, b;
  while (1) {
    s1 ? a;
    s2 ? b;
    max ! (a > b ? a : b);
    min ! (a < b ? a : b);
  }
}
