// a traffic light
// A busy highway is intersected by a little used farmroad
// - Detector C senses the presence of a car on the farmroad
//   - no car on the farmroad, highway light stays green
//   - if vehicle on farmroad, highway light gets yellow and red, so the
//     farmroad light can get green
//   - these stay only green as long as there are cars on the farmroad, but
//     never longer than a set interval
//   - when these are met, the farmroad will turn yellow to red, allowing
//     the highway light to get green
//   - even if farmroad vehicles are waiting, the highway gets at least one
//     a set interval as green.
// - TS generates a short-time pulse (for yellow lights)
// - TL generates a long-time pulse (for green lights)
// - in response to a set (ST) signal

typedef enum {RED, YELLOW, GREEN} LightColor;

statemachine TrafficLightControl {
  init -> highwayGreen;

  output LightColor hw_light;
  output LightColor fr_light;

  // something along the lines of:
  verify foreach YELLOW before RED;

  state highway_green {
    do set_lights(GREEN, RED);
    on time_long() -> highway_yellow;
  }
  state highway_green_and_can_go_to_yellow {
    do set_lights(GREEN, RED);
    on car_at_farmroad() -> highway_yellow;
  }
  state highway_yellow {
    do set_lights(YELLOW, RED);
    on time_short() -> farmroad_green;
  }
  state farmroad_green {
    do set_lights(RED, GREEN);
    on time_long() -> farmroad_yellow;
  }
  state farmroad_yellow {
    do set_lights(RED, YELLOW);
    on time_short() -> highway_green;
  }
}
