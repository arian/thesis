// vending machine

// types of products
typedef enum {COFFEE, TEA, MILK} Product;

// some (not so important) helper functions
uint32 get_product_value(Product p) {
  switch (p) {
    case COFFEE: return 150;
    case TEA: return 100;
    case MILK: return 100;
  }
}

void dispense_product(Product product) {
  // make the machine do it's magic
}

void return_coins(uint32 cents) {
  // make the machine figure out which coins to return
}

// The real state machine
statemachine VendingMachine {

  // amount of inserted money in cents
  uint32 cents;
  // which product the user selected
  Product selected_product;

  // a reusable transition action
  action increment_cents(uint32 coin_cents) {
    cents += coin_cents;
  }

  // initial state, go to idle
  init -> idle;

  state idle {
    // on entering this state, reset the variables
    entry {
      cents = 0;
      selected_product = null;
    }
    // accept coins
    on coin(uint32 coin_cents) / increment_cents -> inserting_coins;
  }

  state inserting_coins {
    // accept more coins
    on coin(uint32 coin_cents) / increment_cents;
    // if selecting a product
    on select_product(Product product) [
      // check if the inserted money is enough
      get_product_value(product) < cents
    ] / {
      // if so, set this product to be selected
      selected_product = product;
    } -> dispense_product; // and go to the product dispenser
    // or cancel, return all the money
    on cancel() -> returning_coins;
  }

  state dispensing_product {
    entry {
      // on entering this state, do the dispensing
      dispense_product(selected_product);
    }
    // epsilon action, directly go to the next state
    on -> returning_coins;
  }

  state returning_coins {
    entry {
      // calculate what amount of (change) money has to be returned
      uint32 cents_to_return = cents;
      if (selected_product != null) {
        cents_to_return = cents - get_product_value(selected_product);
      }
      return_coins(cents);
    }
    // and then go to the idle state, as we're done
    on -> idle;
  }

}

void main() {
  VendingMachine vm;

  vm.coin(50);
  vm.coin(100);
  vm.select_product(COFFEE);
}
