statemachine MySimpleStateMachine {
  init -> off;
  state off {
    on turn_on -> on;
  }
  state on {
    on turn_off -> off;
  }
}
