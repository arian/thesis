// an advanced vending machine, with maintenance and off states

statemachine SimpleCoffeeMachine {

  init -> idle;

  state idle {
    on coin(uint32 cents) [cents > 100] -> dispensing_coffee;
  }

  state dispensing_coffee {
    do {
      make_coffee();
    }
    on ready() [
      // check for the status, as everybody could trigger the ready event.
      is_really_ready_making_coffee()
    ] -> idle;
  }

}

statemachine VendingMachineAdvanced {

  init -> off;

  state off {
    on switch_on() -> on;
  }

  compose state on with SimpleCoffeeMachine {
    state idle {
      on switch_off() -> off;
      on service() -> service;
    }
  }

  state service {
    entry {
      enable_service_light();
    }
    exit {
      disable_service_light();
    }
    on fixed() -> on;
  }

}
