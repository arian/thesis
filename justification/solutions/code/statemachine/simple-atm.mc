// use case:
// bare-bone state machine, it can do some transitions
// it has an initial pseudostate.

statemachine ATM {
  init -> idle;
  state idle {
    on service() -> out_of_service;
    on in(card) -> active;
  }
  state out_of_service {
    on fixed() -> idle;
  }
  on active {
    on cancel() -> idle;
    on done() -> idle;
  }
}
