// compound / junction states.

statemachine MyCompoundSM {
  init -> off;
  state off {
    on startup -> idle;
  }
  state idle {
    on shut_down -> off;
    on card_inserted -> serving_customer;
  }
  junction state serving_customer {
    init -> customer_authentication;
    state customer_authentication {
      on transaction -> idle
    }
  }
}
