// use case:
// after doing a transition, depending on some value
// the next state is chosen.
// after the choice pseudostate, another epsilon transition follows directly.
// transitions in a choice pseudostate don't have an event name, they consist
// only out of guards, a possible action and the goal state.

statemachine ATM {
  init -> idle;
  state idle {
    on in(Card card) -> active;
  }
  state active {
    on pin(uint32 pin) -> check_pin(pin);
  }
  choice check_in(uint32 pin) {
    [pin == 1234] -> state withdraw;
    [true] -> active;
  }
}
