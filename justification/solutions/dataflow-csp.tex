\subsection{Dataflow with CSP}
\label{sect:solutions:dataflow-csp}

A DSL for processes that communicate concurrently through channels

\subsubsection{Problem}

Embedded Systems often have to handle events from the environment. The
application has to react to these arrivals of data.

Additionally it is common for embedded systems to run endlessly, consisting out
of multiple concurrent components that can run at the same time.

Communication between components or input data  is often very tricky, either
through global variables, message queues or mailboxes.

Race conditions, described at~\ref{sect:bugs:race-conditions}, are a source of
errors. The problem with race conditions is that they only happen so often,
under very specific conditions, which makes it difficult to reproduce during
testing.

% Motivation inspirations:
% - PRET-C: A New Language for Programming Precision Timed Architectures
% - http://homepages.engineering.auckland.ac.nz/~roop/pub/2009/andalam09.pdf
% - ReactiveC
% - Generic Tools, Specific Languages - Mbeddr mentions concurrency as an open
%   problem

\subsubsection{Related Work}

\paragraph{Hoare CSP}

In \cite{hoare1978communicating} Hoare describes Communicating Sequential
Processes (CSP). It is a formal language to describe concurrent processes
that communicate with input and output commands. Those commands can be composed
sequentially, in parallel, with a guard expression or repetitively.

\paragraph{Kahn Process Networks}

Kahn Process Networks are a model to express behavior of data flow. Using
process networks, an application is modelled as a collection of concurrent
processes communicating through FIFO channels.

\paragraph{Occam}

Occam~\cite{may1984occam} is a concurrent programming language based on CSP\@.
Occam is based on the CSP model of computation.

Occam programs programs are constructed from three primitive processes:
assignment, input and output: \texttt{v := e}, \texttt{c~?~v} and
\texttt{c~!~e} respectively. Input and output processes read from and write to
channels.

Constructors are used to combine processes to form larger processes.
\texttt{SEQ} is the sequential constructor that executes processes
sequentially. \texttt{PAR} executes processes in parallel. The alternative
constructor \texttt{ALT} chooses one of the component processes for execution.
\texttt{IF} and \texttt{WHILE} constructors are also provided.

The code example~\ref{lst:solutions:dataflow-csp:occam} shows some occam code

\begin{listing}[H]
  \begin{verbatim}
CHAN OF INT c:
INT x:
PAR
  c ! 5             -- write 5 to the channel
  SEQ
    c ? x           -- get a value
    consume(x)      -- in some way
  \end{verbatim}
  \caption{Occam producer/consumer example}
  \label{lst:solutions:dataflow-csp:occam}
\end{listing}

\paragraph{Handel-C}

% see http://essay.utwente.nl/58152/1/scriptie_J_van_Zuijlen.pdf
% http://www-users.cs.york.ac.uk/~susan/bib/ss/occam/357.pdf
Handel-C~\cite{handel-c} is an C based hardware description language targeting FPGAs. It a
C-like language extended with CSP like occam, it has a \texttt{PAR} construct,
channel communication and an \texttt{ALT} construct.

The code in~\ref{lst:solutions:dataflow-csp:handel-c} shows some code of
parallel processes that communicate over a channel.

\begin{listing}[H]
  \begin{minted}{c}

  int main() {
    chan int c;
    int x;
    par {
      c ! 5;
      {
        // this block is sequential
        c ? x;
        assert(x == 5);
      }
    }
  }
  \end{minted}
  \caption{XC code with two parallel tasks}
  \label{lst:solutions:dataflow-csp:handel-c}
\end{listing}


\paragraph{XC}

% see https://www.xmos.com/published/xmos-programming-guide
XC~\cite{xc} is a programming language for real-time embedded parallel
processors. It targets XMOS XCore processor architectures. The language extends
C with occam-like parallelism primitives.

Code~\ref{lst:solutions:dataflow-csp:xc} shows an example of XC code with two
parallel tasks that communicate through a channel.

\begin{listing}[H]
  \begin{minted}{c}
  chan c;

  void producer (chanend c) {
    c <: 5;
  }

  void consumer (chanend c) {
    select {
      case c :> int i:
          printintln(i);
          break;
    }
  }

  int main() {
    // these two tasks will run in parallel
    par {
      producer(c);
      consumer(c);
    }
  }
  \end{minted}
  \caption{XC code with two parallel tasks}
  \label{lst:solutions:dataflow-csp:xc}
\end{listing}

\paragraph{KRoC}

KRoC is the Kent Retargetable occam Compiler. It is an occam-pi, an extended
version of occam, compiler. KRoC can run occam-pi programs  using the CCSP
runtime, a highly-efficient concurrent runtime or using the Transterpreter,
some kind of virtual machine for occam-pi programs.

\paragraph{tinycsp}

TinyCSP~\cite{tinycsp} is a CSP-ish channel implementation using GOTOs and some
macros. It is a small, plain C experimental implementation that is designed to
run on small devices.

\paragraph{libcsp}

libcsp~\cite{Beton00} is a library that provides for C programmers a level of
abstraction that makes programming with threads much easier. By providing an
easy mechanism to express and reason about programs with multiple threads, it
becomes easier to fit software into the real world.

\paragraph{Formal Verification}

CSP models can be formally verified by various verification tools. The goal is
to generate a model of one of the tools, so a tool can check if the program has
deadlock-freeness, livelock-freeness or satisfies some Linear Temporal Logic
(LTL) properties.

\begin{description}
  \item[FDR] Failures-Divergences Refinement is a tool for analyzing
    programs written with CSP.
  \item[PAT] Process Analysis Toolkit is a framework for simulating and
    reasoning of concurrent real-time systems. PAT implements various model
    checking techniques for different properties such as deadlock-freeness,
    reachability, LTL properties and refinement checking.
  \item[SPIN] is a popular software verification tool that can be used for
    formal verification of multi-threaded software applications. It uses the
    Promela language to define models, which has resemblances of CSP.
\end{description}


\subsubsection{Solution}

To easily create concurrency and manage the flow of data through the
application, a CSP-like textual extension to C is proposed. It consists out of
two main additions: processes and channels. Processes represent computational
blocks that are connect with each other through channels.

\paragraph{Processes}

Defining a process is done using the \texttt{process} keyword, as shown in
code~\ref{lst:solutions:dataflow-csp:process}.

\begin{listing}[H]
  \inputminted{c}{solutions/code/dataflow-csp/00-process.mc}
  \caption{A simple process function}
  \label{lst:solutions:dataflow-csp:process}
\end{listing}

This process does not do anything, and when instantiated, will immediately
terminate. Processes can be instantiated by the \texttt{par} keyword described
in~\ref{par:solutions:dataflow-csp:parallel}. Like normal C functions, a
process can have parameters, so when instantiating a process, it is possible
to pass data into the process, for example channels to communicate with other
processes or any other data.

\paragraph{Channels} \hfill

Adding a parameter to the process with the type \texttt{chan<int>}, creates an
input channel of \texttt{int}s.
Example~\ref{lst:solutions:dataflow-csp:producer} is a continuous process, that
writes an incremented value to the channel. Writing to a channel is denoted as
\texttt{channel~!~expression}. Channels are rendezvous, meaning that the writer
blocks until there is a reader.

\begin{listing}[H]
  \inputminted{c}{solutions/code/dataflow-csp/01-P.mc}
  \caption{Writing to a channel}
  \label{lst:solutions:dataflow-csp:producer}
\end{listing}

Using libcsp, the translated C code will look
like~\ref{lst:solutions:dataflow-csp:producer-libcsp}. A generated process is
defined as a function with a \texttt{Process} object. This object contains the
original arguments passed into the initialization of the process.

\begin{listing}[H]
  \begin{minted}{c}
#include <process.h>
#include <csp.h>
typedef struct  {
  Channel *out;
} P_ProcessArgs;

void P (Process *p) {
  P_ProcessArgs *args = p->args;
  Channel *out = args->out;
  {
    signed int i = 0;
    while (1) {
      CSP_chanOutInt32(out, i++);
    }
  }
}
  \end{minted}
  \caption{Generated code using libcsp}
  \label{lst:solutions:dataflow-csp:producer-libcsp}
\end{listing}

The producer will unblock if a value is read from the channel. Reading from a
channel is denoted as \texttt{channel~?~var-ref}, as shown in
example~\ref{lst:solutions:dataflow-csp:consumer}.

\begin{listing}[H]
  \inputminted{c}{solutions/code/dataflow-csp/02-C.mc}
  \caption{Reading from a channel}
  \label{lst:solutions:dataflow-csp:consumer}
\end{listing}

The following example~\ref{lst:solutions:dataflow-csp:id} is the \texttt{ID}
process. It has one input and one output channel, and copies values from the
input to the output.

\begin{listing}[H]
  \inputminted{c}{solutions/code/dataflow-csp/03-ID.mc}
  \caption{ID Process}
  \label{lst:solutions:dataflow-csp:id}
\end{listing}

All writes to a channel are strictly by value. The consumer may change the
output value from a channel, which are copies from the inputs, but these
changes cannot affect the values of the inputs, However it is possible to pass
a pointer, but like passing pointers into function calls, the programmer has to
understand the consumer may change the value of the object the pointer is
pointing to.

This semantic choice for this semantic  has two reasons. The first one is that it works the same as
calling C functions, where the arguments are copied as well. Secondly, like
with calling functions, it is possible to pass a pointer to another object
if that would be necessary, either because of efficiency reasons because your
object is too big or because the consumer has to change the value of the object
the pointer is pointing to.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/10-pass-by-value.mc}
  \caption{Passing values over a channel by value}
  \label{lst:solutions:dataflow-csp:pass-by-value}
\end{listing}

The program in~\ref{lst:solutions:dataflow-csp:pass-by-value} shows the
described behavior. This program has two channels running in parallel (using
\texttt{par} described in~\ref{par:solutions:dataflow-csp:parallel}).
The producer writes to two channels. One channel is of the
type \texttt{struct MyData}, and the other one has a pointer of the structure:
\texttt{struct MyData *}. In the consumer the field \texttt{x} is overwritten.
After an extra \texttt{sync}, the data is printed. With the pointer channel,
the original object is indeed modified, as \texttt{2} is printed in both the
consumer as producer. However with the channel where the object is passed by
value, the original value is printed in the producer, while the version of the
consumer prints \texttt{2}.

\paragraph{Parallel Processes}
\label{par:solutions:dataflow-csp:parallel}

The previous described processes \texttt{P}, \texttt{C} and \texttt{ID} can be
linked together by channels. They need to be instantiated concurrently. This is
possible with the \texttt{par~\{~processes~\}} statement.

An example is the \texttt{copy} process in
example~\ref{lst:solutions:dataflow-csp:copy}. It creates to channels, and
links the \texttt{producer} with the \texttt{ID} process, which is linked to
the \texttt{consumer} process.

\begin{listing}[H]
  \inputminted{c}{solutions/code/dataflow-csp/04-copy.mc}
  \caption{A copy process, using construction process using \texttt{par}}
  \label{lst:solutions:dataflow-csp:copy}
\end{listing}

The \texttt{par} statement initializes and runs the processes. It will block
further execution until all processes have terminated.

\paragraph{Alternative channel selection}

An alternative statement, \texttt{alts}, selects one channel for input. It
looks similar to a \texttt{switch} statement but with cases referring to
channels.

A \texttt{case} is of the form
\texttt{case~<guard>~<communication>:~<statement>}.  The \texttt{guard} part is
optional. If it is given, inside square brackets, the expression should not be
\texttt{0} for the \texttt{case} to be selected.  The \texttt{communication}
can have two forms: just reference to a channel or an channel read expression.
Finally if there is no guard or the guard is unequal to \texttt{0}, and the
channel can communicate, the \texttt{statement} is executed.

If one of the channels is ready for input, that case will be selected. If
it has a read expression, the read expression will assign the value from the
channel to variable. Then the statement will be executed.

A \texttt{default} case can be added if none of the other cases can be
selected.

The \texttt{alts} statement will block, if there is no default case, until
one of the case is executed.

An example of the \texttt{alts} statement is given
in~\ref{lst:solutions:dataflow-csp:alts}.

\begin{listing}[H]
  \inputminted{c}{solutions/code/dataflow-csp/06-alts-counter.mc}
  \caption{An alts statement, keeping a counter as state, counting either up
    or down, depending on the `firing' channel. The guard for the `up' case
    ensures that the counter will not be greater or equal to \texttt{10}.}
  \label{lst:solutions:dataflow-csp:alts}
\end{listing}

Another example is give in~\ref{lst:solutions:dataflow-csp:alts-terminate}

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/09-terminate-while.mc}
  \caption{Using \texttt{alts} to either receive a new character or to stop
    the while loop to terminate the process}
  \label{lst:solutions:dataflow-csp:alts-terminate}
\end{listing}

\paragraph{Buffer Process}

In embedded software, interrupt service routines (ISR) are triggered when
something happens. These can happen at any point in time and are typically
small. These ISRs should not block, so writing to a channel should be
non-blocking, contrary to normal rendezvous channels.

In~\cite{hoare1978communicating} a buffering process is described. It is a
FIFO buffer with a given size. The process uses an array of a certain size,
an index of the location to write to in the array and an index to read from the
array. The buffer can either be empty or not and full or not. If the buffer is
full, it is not possible to add any more values. If the buffer is empty, it is
not possible to retrieve values from the buffer.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/11-guarded-alt.mc}
  \caption{A FIFO buffer}
  \label{lst:solutions:dataflow-csp:fifo-buffer}
\end{listing}

The program in~\ref{lst:solutions:dataflow-csp:fifo-buffer} shows an example of
a implementation. The \texttt{P} process is a producer that irregularly
produces some data. The \texttt{C} process is a consumer that can only consume
one value each second. The \texttt{buffer} process is connected to the producer
and consumer. It contains a local variable \texttt{buf} which will hold the
buffer values. Using an \texttt{alts} statement it handles new values from the
producer or readings from consumer. The communication with the consumer needs
an additional channel to signal that the consumer is ready, the
\texttt{request\_c} channel. This is necessary as alts-cases can only read
from channels, and not write to a channel.\footnote{for now at least}

The first part of output of this program looks as follows. The first column
are seconds, and the second column are the values.

\begin{verbatim}
53 0
54 1
55 2
56 3
57 4
63 5
64 6
65 7
66 8
67 9
73 10
74 11
\end{verbatim}

What you can see is that each second the consumer prints a number for the first
5 numbers. The producer only put 5 numbers into the buffer, so the buffer is
empty. After a short timeout, the producer puts another 5 numbers into the
buffer after which the consumer can consume new numbers again, one each second.

\paragraph{Buffered Channels}

This is still complex, instead we want to be able to readily create buffered
channels. Adding a storage specifier when defining the channel should solve
that: \texttt{buffered[10] chan<int>} will create a buffered channel of
\texttt{int}s of size 10. Program~\ref{lst:solutions:dataflow-csp:fifo-buffer}
can be refactored to~\ref{lst:solutions:dataflow-csp:buffered-channel}.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/12-buffered-channel.mc}
  \caption{Instead of a buffer process, simply use a buffered channel}
  \label{lst:solutions:dataflow-csp:buffered-channel}
\end{listing}

One addition to this latest program is the addition of the \texttt{default}
case in the \texttt{alts} statement. This way the \texttt{alts} statement will
not block if there is nothing to read from the buffer, and will instead execute
the \texttt{default} block.

\paragraph{One-to-one Channels}

All channels are one-to-one. Meaning that when there are two processes waiting
for the channel to communicate, only one will receive the message. The other
processes will be blocked until another value is written to the channel. Which
process will run first depends on which comes first. Consecutive writes will
activate all processes in round-robin fashion.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/13-multi-readers.mc}
  \caption{A one to many channel configuration}
  \label{lst:solutions:dataflow-csp:multi-readers}
\end{listing}

Code~\ref{lst:solutions:dataflow-csp:multi-readers} shows a configuration of
one counter that puts values on a channel, connected to multiple readers. One
of the readers will receive the first value, followed by the others. The
readers run in a round-robin fashion. A output of this program could be:

\begin{verbatim}
$ ./multi-readers
B 0
A 1
C 2
D 3
B 4
A 5
C 6
D 7
\end{verbatim}

Instead of multiple readers of a channel, it is also possible to create a
configuration where multiple writers write to a channel, with only one reader.
Such a configuration is shown in~\ref{lst:solutions:dataflow-csp:multi-writers}.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/14-multi-writers.mc}
  \caption{A many to one configuration}
  \label{lst:solutions:dataflow-csp:multi-writers}
\end{listing}

As with the one to many example, a round-robin strategy is applied, with the
writers.  The output of this program could be as follows, where the lines with
\texttt{P} is the printer process, that receives the character from one of the
writers.  The lines without the \texttt{P} are printed by one of the writer
processes:

\begin{verbatim}
$ ./multi-writers
P C
P B
B
C
P A
A
P B
B
P C
P A
C
A
P B
B
\end{verbatim}

Notice that the \texttt{printf}s are not always executed in the same order.
This is because only the communications between the processes are synchronized.
To enforce a periodic sequence, a second channel can be added, with a
\emph{write} after the \texttt{printf} of the \texttt{print} process, and a
\emph{read} before the \texttt{printf} of the \texttt{writer} process.

\paragraph{Fan-out and Fan-in}

In the above paragraph it was shown that channels are one-to-one. To send one
value to multiple processes, it is possible to create an intermediate process
that receives a value from the original channel, and sends it out to multiple
channels. An example of such a process is shown in
code~\ref{lst:solutions:dataflow-csp:fan-out}.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/15-fan-out.mc}
  \caption{A process that receives one value and sends it onto multiple other
    channels}
  \label{lst:solutions:dataflow-csp:fan-out}
\end{listing}

The opposite, merging the values of multiple channels into one channel, is
also possible using the \texttt{alts} construct, as shown in
code~\ref{lst:solutions:dataflow-csp:fan-in}.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/16-fan-in.mc}
  \caption{A process that receives multiple values and sends it onto one
    channel}
  \label{lst:solutions:dataflow-csp:fan-in}
\end{listing}

\newpage

\paragraph{Verification and analysis}

An advantage of using CSP, is that formal verification tools exist to verify
the correct behavior of the program. One tool is PAT, which can simulate and
check for certain properties such as deadlock freeness and reachability.

PAT models are written in CSP$\sharp$. After generating MetaC CSP into
CSP$\sharp$ it will be possible to start analyzing your program.

Currently, only a proof-of-concept exists which can generate a simple MetaC CPS
program into CSP$\sharp$.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/17-simple-csp.mc}
  \caption{A simple program that can be transformed into CSP$\sharp$}
  \label{lst:solutions:dataflow-csp:simple-csp}
\end{listing}

The code~\ref{lst:solutions:dataflow-csp:simple-csp} will generated to
CSP$\sharp$ such as code~\ref{lst:solutions:dataflow-csp:simple-csp-sharp}.

\begin{listing}[H]
  \verbatiminput{solutions/code/dataflow-csp/17-simple-csp.csp}
  \caption{A simple program transformed into CSP$\sharp$}
  \label{lst:solutions:dataflow-csp:simple-csp-sharp}
\end{listing}

A graph of a simulation is shown in image~\ref{fig:solutions:dataflow-csp:simulation-graph}

\begin{figure}[H]
  \centering
  \includegraphics[width=2.5cm]{solutions/code/dataflow-csp/simple-csp.png}
  \caption{A simulation of the CSP program}
  \label{fig:solutions:dataflow-csp:simulation-graph}
\end{figure}

When checking if the model is deadlock free, the verification results, as it
can be seen in the graph, that this property is satisfied. If instead from
example~\ref{lst:solutions:dataflow-csp:simple-csp} in the \texttt{C} process
the \texttt{x} channel is read before the \texttt{c} channel, the resulting
CSP$\sharp$ model will change, and the verification will notice a deadlock in
this system.

\paragraph{Process State}

Often a process runs indefinitely, using an infinite loop, where the
communication blocks the process until some some communication can be done.
State of the process is kept in a local variable. For modeling purposes it
would be better if this would have a more formal denotation, so the state of
a process can be included in the model.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/18-state.mc}
  \caption{The processes \texttt{print} and \texttt{a} both have a state
    variable called \texttt{count}.}
  \label{lst:solutions:dataflow-csp:state}
\end{listing}

In~\ref{lst:solutions:dataflow-csp:state} the processes \texttt{print} and
\texttt{a} have state. State is given to a process after the parameters,
divided by a \texttt{@} sign (phonetically process X at state Y). Initializing
a new process (in a \texttt{par} block) with state is similar, first the
arguments, the \texttt{@} sign and the initial state for the process. When
calling the process recursively (to create a loop), just the arguments will become
the new state.

\begin{figure}[H]
  \centering
  \includegraphics[width=3.5cm]{solutions/code/dataflow-csp/state.png}
  \caption{A simulation of the CSP program with state}
  \label{fig:solutions:dataflow-csp:simulation-state}
\end{figure}

In figure~\ref{fig:solutions:dataflow-csp:simulation-state} the simulation of
the model is shown.

\paragraph{Static variables in processes}

As running the process continuously is done by calling the process again, the
entire process body will be executed again. Sometimes however, you want to
initialize something, or keep variables that are not part of the state
parameters.

\begin{minted}{c}
process f() {
  static int x;
  process_init: {
    x = 0;
  }
  x++;
  f();
}
\end{minted}

Adding the \texttt{static} keyword to a variable declaration it
will keep it's value after calling the process. To initialize a variable once,
just when the process is run for the first time, the \texttt{process\_init}
label can be used.

\paragraph{Modeling constraints}

As C code can be mixed and matched with the CSP constructs, special attention
is necessary by the programmer to generate a useful model.

\begin{itemize}
  \item Values written over a channel are assumed to be integers. Boolean and
    integers are the only data types PAT supports by default. Other types work
    fine, but it is not considered in the model.
  \item The state of a model can be passed as \emph{state parameters}. These
    parameters are defined as \texttt{process name(t1 param1, t2 param2 @ s1
      state1, s2 state2)}. The state variables will be in the model. Changing
    the state can be done by calling the process as \texttt{name(new\_state1,
      new\_state2)} at the end of the process. The process will be executed
    again with the new state.
  \item The test expression of \texttt{if} statements can only depend on state
    variables or simple arithmetic expressions. Then the model can make a
    deterministic choice, otherwise the model will be nondeterministic, meaning
    the statement could be executed or not. According to the model, it will be
    possible to reach certain states, that might falsify the properties you
    like to test, while in practice those could not happen.
  \item The same constraint is true for guard expressions of \texttt{alts}
    cases.
  \item Communication should not be done in loops, instead use a recursive
    call to execute the process again.
  \item Other statements, that are not relevant or known to the model are,
    ignored.
  \item Model checking only works with finite state space. So a recursive call
    with a continuously increasing counter will not work, as there will be a
    infinite number of states which a model checker usually cannot handle.
\end{itemize}

\paragraph{Connecting to inputs and interrupts}

When receiving inputs using polling or using a function that blocks until there
is a new value, normal processes as described here are perfectly possible.
However interrupt service routines are a bit different. First they are
initialized and called by the operating system and secondly they need to
execute quickly without blocking. This is not true for the described extension
so far: processes created by \texttt{process} are handled by the runtime, and
normal communication over channels can block until the other end is ready.

\begin{listing}[H]
  \inputminted[fontsize=\footnotesize]{c}{solutions/code/dataflow-csp/19-buffered.mc}
  \caption{Example with ISRs and channels}
  \label{lst:solutions:dataflow-csp:isr}
\end{listing}


