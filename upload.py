#!/usr/bin/python
import ftplib
import os
import sys

folder = sys.argv[1]
filename = sys.argv[2]
password = sys.argv[3]

ftp = ftplib.FTP("ftp.aryweb.nl")
ftp.login("aryweb", password)
ftp.cwd("/domains/aryweb.nl/public_html/thesis")
os.chdir(folder)
myfile = open(filename, 'rb')
ftp.storbinary('STOR ' + filename, myfile)
myfile.close()
