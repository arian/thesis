Embedded DSL in Haskell for Embedded Systems
--------------------------------------------

Use it to generate 'safe' code, type-check, property check for propositions...


Rust
----

It's a systems programming language

It can be functional, pattern matching, immutable properties, but also very low
level.

Use it to program embedded systems. Depends on which type of embedded system,
which level of low or high level hardware.

Maybe it's possible to write some kind of extension that integrates with
QuickCheck to test properties of functions.

House
-----

A Operating System written in Haskell

http://programatica.cs.pdx.edu/House/


Erlang
------

Erlang is a kind of a functional programming language, especially for
concurrent programming, using actors/message passing. For many communicating
embedded systems, in some kind of cluster, this could be a solution.

The Curry-Howard Isomorphism
----------------------------

The Curry-Howard Isomorphism is a relationship connecting type-theory and
structural logic.

http://en.wikibooks.org/wiki/Haskell/The_Curry%E2%80%93Howard_isomorphism


Wireless data transmission with LEDs
------------------------------------

With Marco

http://www.ted.com/talks/harald_haas_wireless_data_from_every_light_bulb

IoT at Embedded Software
------------------------


Sensor Networks
===============

Regional Streams: Functional Macroprogramming for Sensor Networks
-----------------------------------------------------------------

Use functional programming, immutability etc. for macroprogramming of sensor
networks: write the global program of the entire network, instead of writing
programs for the individual nodes.

MetaC
=====

Create a subset of the C programming language, that is extended with new
modern features and DSLs to make programming of embedded systems better.

The language will be implemented with Spoofax.

It will be similar to mbeddr.

### Features

- State Machine
- Modules
- Better Types (explicit size (u8, i32, booleans, etc.)), this can help
  compiler optimizations, and warnings:
  - uninitialized variables
  - `if (x = y)` -> should throw a warning
  - `x / y` integer division
  - dead code
- No pointer arithmetic
- No macros
- SMT (Satisfiability Modulo Theories) formal verification
- QuickCheck properties checker (DSL, compile to Haskell QuickCheck or ScalaCheck with C ffi)
- Pure functions without side-effects?
- Interoperability with existing C code
- Fully textual
- Concurrency helpers (immutables, mutexes, mailboxes, volatile)
- Require a default case for switch (or handle all cases (like Scala match has))
- Lambdas
- Debugging
- Analysis (Power consumption, performance)
- WCET single path predictable code transform. (http://www.vmars.tuwien.ac.at/documents/extern/884/paper-puschner-dipes-2002.pdf)

### Approach

1. Describe the current state of embedded systems programming.
2. Implement the features on top of (Base)C.
3. Use the features in real embedded projects.
4. Compare approaches of the old-style C vs. MetaC.


### Some common language rules

- Better Embedded System Software (Ch. 17)
  - switch must have a default case
  - no assignments allowed in conditionals: (if (x = 1))
  - pointers passed into a subroutine must be null-checked
  - defined constants must be used instead of numeric constants
  - MISRA C
    - A set of software guidelines for the C programming language
- Global Variables Are Evil (Ch. 19) - Maybe only allow quasi globals

### mbeddr shortcomings

#### lambdas can't see values from the outer scope

For example in this code

```c
void main() {
  uint32 y = 4;
  doSthWithAPosition([p|
    p->x = y;
    p->y = y;
    p;
  ], x);
}
```

This won't work, because the lambda passed into doSthWithAPosition can't see
the `y` variable. The generated C code creates a anonymous function next to the
main function, so doesn't have access to the variables in the main scope.
However as it's a generated anonymous function, the accessed variables could be
detected, and passed as pointers into that function as extra parameters.

#### Plain text tools (for version control)

All source files of mbeddr models are XML files. This eliminates all existing
tools that are already available that work with plain text, for example many
unix tools such as `grep` or `diff` or version control tools such as `git`.

### Verification of timing (timed automata)

Maybe by make use of the DSL features it would be possible to generate an
[UPPAAL](http://www.it.uu.se/research/group/darts/uppaal/small_tutorial.pdf)
model that can be model checked.

This tool is about timing. Embedded Systems often have to deal with timing of
tasks and interrupts. So maybe this tool can verify that the system under
verification is correct, and there are no deadlocks, priority inversion and
that types of errors.


### Meeting 2015-03-10

Things to do for the next few weeks:

- Find real projects that are representative and identify common bugs that
  could be prevented by special language features. This will support the new
  features by some real numbers and evidence.

- Improve the Spoofax implementation of C.

- Import the QuadCopter code into mbeddr. Make it work. Eventually the
  QuadCopter code can also be refactored for MetaC, so both implementations
  can be compared.

- Make things more concrete.

### Meeting 2015-04-01

In the last weeks I've been searching for embedded software projects that I can
use to justify language decisions.

I did found some projects and issues/bugs, that I wrote down as footnotes in
the document I sent.

However it was difficult to find good representative projects. To decide what
representative or good actually means, it would be a good idea to describe
this: what are the requirements for these projects. Problems I encountered was
that there were no reported bugs, no activity, or the project was some kind of
small hobby project. Also I probably won't find big industry projects, as they
are usually not open source.

I will write down the methodology I used for searching projects. Which search
tool (GitHub Search), which keywords (by RTOS, hardware, goal), etc.

For the projects that meet the criteria, I will write a description for each of
them, with the goal of the project, the size (number of files, developers,
reported bugs, pull request, etc.).

With the projects it is possible to find bugs, and what's going wrong with C.
I can use this to make C safer. It is a lot harder to come up with abstractions
that can be captured in a DSL.

To justify those abstractions I could give code examples, that it becomes
almost obvious that it is nicer. But 'nicer' is not really quantifiable.

For the next few weeks, besides completing the things above, I will try to
convert the my quadcopter student project into mbeddr. In advance I can define
the requirements, or what the most difficult problems were. An example would be
that all engines should be off in the calibration state. Keeping a log when
importing the project will indicate all interesting points. Using the mbeddr
DSL features as much as possible can give an idea what's useful. An interesting
thing to do, what would be an advantage of an abstraction, is to use the
verification tools to verify that indeed the engines are off during an
calibration state.

The next meeting, with hopefully Koen and Guido, will be in the week of April
20-24.


### TODO List 2015-04-22

- Find one or two extra projects to analyze
- Describe the projects
- Describe the process of finding the projects, criteria
- Refactor the mbeddr quadcopter implementation to use more mbeddr concepts
- Design DSLs
  - Component (box/wire/timing)
  - Statemachine
  - Physical Units
  - etc.

### Before 2015-04-22 Meeting

I do not have a lot to discuss. I still need to describe how I found the
projects on GitHub. I need to find one or two more. Also I still need to
describe these projects, the size, goal, etc.

Last week I have been working on porting my Quad Copter project over into
mbeddr. It seems there is no quick import, so it's very labor intensive. At the
moment I've ported everything verbatim, and am trying to fit the Statemachine
and Component DSLs in the program. I took notes of any potential interesting
problem that occurred.

I expect it will take two or three weeks to finish this.

I think after I'm done with the two tasks above, I can start designing my own
DSLs. At that point I have experience with a similar language (mbeddr), have
analyzed several embedded projects, and looked at literature (Hume, Giotto).
It's probably a good idea to do this very systematic. For example for each
feature to describe the problem (with a code-example from one of the projects),
similar solutions (if available), the proposed solution and an example how this
solved the problem of the code-example.

It can happen that I need more real-world code examples, or need more
literature, and I have to go back to search for more, but I think a more
iterative workflow is more productive than a waterfall workflow where I try to
find all problems first, and then find solutions.

### Meeting 2015-04-22

We've discussed what I've been doing.

- Describe projects
- Find fundamental changes in the projects.
  - Switched from or to a State Machine, did it work etc.
  - Ask maintainers directly.
- Refactor the mbeddr QuadCopter project
  - What are the pain points?
  - What does mbeddr not cover?
- Decide which features I want to create.
  - And justify.



