\chapter{Concurrency}
\label{ch:concurrency}

It is common that embedded systems have multiple components that run
continuously alongside each other and need to communicate to work together.
The concurrency extension adds language features to create parallel processes
and synchronous channels for communication.

\section{Background}

Embedded systems often consist of multiple concurrent components, Real Time
Operating System (RTOS) tasks or interrupt service routines (ISR), that have to
communicate with each other. A component has to let other components know
something happened or share some data. Interrupts are usually the initial
source of data that needs to be processed by the system, flowing into the
various components.

One of the easiest methods to share data between components is using global
variables. However this introduces the risk of concurrency bugs. Maybe the most
common type of bugs occur when a data value that is multiple machine words long
is read by a task, and is changed by another task at the same time. As the read
is not atomic, the final result may contain some data from before the write by
the other task, and some new data from after the write that makes the result
invalid. When global variables are used it is important to protect the variable
with a mutex or disable interrupts temporarily to avoid concurrency
problems~\cite{koopman2010better}. Using mutexes manually can sometimes be
hard to use and is prone to bugs because there is no control of their proper
usage.  Also there is no connection between the data controlled by the mutex
and the mutex.

The concurrency extension adds features for defining concurrent processes and
channels to make communication between concurrent processes easier to program.
Processes can read from and write to channels to communicate with each other.
When a process is ready to write to a channel and another concurrent process is
ready to read from this channel the communication is performed and both
processes continue with further execution.

The extension design is influenced by Communicating Sequential Processes (CSP)
\cite{hoare1978communicating}. CSP is a language for defining the interaction
of concurrent systems. It has influenced many languages since, including
Occam~\cite{may1984occam} and Go~\cite{pike2012}.

As CSP is also used as a process algebra, it is applied as a method for
specifying and verifying concurrent systems.  As concurrency bugs do only
appear so often, tests might not catch the bug, but formal verification can
find those issues. Using new syntax features to define the processes and the
communication between processes, verification model specifications can be
generated directly from the MetaC code.

The extension introduces new syntax for processes and channels that do not have
a direct equivalent syntax in plain C or mbeddr. Instead the extension uses a
small C runtime library.

%\begin{itemize}
%  \item Introduction, motivation, multiple concurrent processes that need to
%    communicate, safe rendez-vous communication, analysis and model checking
%  \item Extension is interesting because mbeddr does not include it, it
%    requires a small runtime, non-trivial mapping from MetaC to C and can
%    generate model checking models.
%  \item Related Work, Hoare CSP, Occam.
%  \item Small producer / consumer example
%\end{itemize}

\section{Code Example}

The following example shows a simple controller that could be found in a drone.
During development the drone receives data through a \emph{RS232} connection.
When data is received the RS232 ISR is triggered. Depending on this data the
drone should change the settings of the four rotors.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{pics/concurrency-drone.pdf}
  \caption{A diagram of the example system.}
  \label{fig:csp:drone-diagram}
\end{figure}

\autoref{fig:csp:drone-diagram} shows a diagram of the example system. When the
RS232 ISR is serviced it passes a value to the \texttt{receive\_command}
processes that decodes commands, the decoded command is then passed to the
\texttt{controller} processes that translates the command into rotor settings
which are sent to the \texttt{rotors} process that controls the rotors.

The following paragraphs, will briefly explain the code of the example while
the following sections will explain the features of the concurrency extension
in detail.

\begin{figure}[H]
  \inputminted{c}{code/csp/init.mc}
  \caption{Running processes concurrently.}
  \label{fig:csp:drone-init}
\end{figure}

In \Cref{fig:csp:drone-init} the three processes are started concurrently with
a \texttt{par} statement. The statement is used in an ordinary C function that
happens to be the \texttt{main} function of the program in this case. Besides
starting the processes, the channels that provide the communication between the
processes are declared as well. These channels are then passed into the
processes as the process arguments. The \texttt{@} sign in the arguments list
of \texttt{receive\_command} call separates process arguments and the initial
values of the \emph{state variables} for the process, as will be explained
shortly. Furthermore, the function sets an interrupt service routine to receive
the data from the RS232 connection.


\begin{figure}[H]
  \inputminted{c}{code/csp/rotors.mc}
  \caption{A drone rotors controller. Two processes communicating over a
    channel.}
  \label{fig:csp:drone-rotors}
\end{figure}

\autoref{fig:csp:drone-rotors} shows the code of the \texttt{controller} and
\texttt{rotors} processes. The \texttt{controller} process has two parameters:
both are channels, one with \texttt{SetPoints} values, specifying what the yaw,
roll, pitch and speed of the drone should be. The other channel is for
\texttt{RotorSettings} values that are the rotation speeds for each of the four
rotors. Inside the body of the process \texttt{SetPoints} values are read from
the \texttt{setpoints\_channel} using the \texttt{?}\ expression. This
expression blocks the execution of the process until a value is available and
writes it to the \texttt{setpoints} variable.  The
\texttt{calculate\_rotor\_speeds} function is a library function that
calculates the rotor settings depending on the setpoints. In the following step
of the process these rotor settings are written to the
\texttt{settings\_channel} using the \texttt{!}\ expression. Like the
\texttt{?}\ expression this expression blocks until there is another process
that can read from this channel so the communication can be executed. The final
step of the process is calling the process, which will create a loop that
causes the process body to run again. This method of looping helps the compiler
analyze the program and generate verification models
(\Cref{sec:concurrency:verification}).

The \texttt{rotors} process has a channel of \texttt{RotorSettings} values as
parameter, the \texttt{?}\ expression reads the value and writes the value to
the \texttt{settings} variable, which is then passed into a library function
\texttt{set\_rotor\_speeds}.

\begin{figure}[H]
  \inputminted[fontsize=\small]{c}{code/csp/isr.mc}
  \caption{Receiving RS232 data.}
  \label{fig:csp:drone-isr}
\end{figure}

Channels can also be declared globally so any function, including ISRs can
write values to channels, as shown in \Cref{fig:csp:drone-isr}. The channel is
buffered, that means writes to the channel do not block. This is very important
for ISRs as the execution of this function should be as fast as possible.

The \texttt{receive\_process} is a separate process that reads from a channel
of characters and translates this into setpoints. A command consists of two
bytes, the first byte determines which value needs to be changed, and the
second byte is the value.  These bytes are read with the two read (\texttt{?})
expressions. Depending on the value of the \texttt{standby} state parameter,
the process is either in standby or not. If the system is in standby, it will
only consider the \emph{standby command}.  Otherwise it can receive the new
setpoint values.  The \texttt{static} \texttt{setpoint} variable persists when
the process is looped, when receiving a new value the old value of the object
should updated.  At the end of the process the \texttt{receive\_command}
process is called to create a loop. The new standby value is passed as argument
to set the new standby state of the process.

The recursive call and state parameters are designed to help formal
verification, discussed in \Cref{sec:concurrency:verification}, as this code
structure declares loops and state variables that can be translated to a
verification model.

%\begin{itemize}
%  \item Explain example
%\end{itemize}

\section{Definition of Processes}
\label{sec:concurrency:processes}

Defining a process is done using the \texttt{process} keyword, as shown in
\autoref{fig:csp:process}.

\begin{figure}[H]
  \inputminted{c}{code/csp/empty-process.mc}
  \caption{A simple process declaration.}
  \label{fig:csp:process}
\end{figure}

This process prints two numeric sequences. Variables \texttt{a} and \texttt{b}
are not changed, while \texttt{c} and \texttt{d} are updated by the recursive
call.

Processes can be instantiated by the \texttt{par} keyword described
in~\Cref{sec:concurrency:par}. Like normal C functions, a process can have
parameters, so when instantiating a process, it is possible to pass data into
the process, for example channels to communicate with other processes or any
other data.

In embedded systems processes often run indefinitely using an infinite loop.
To capture this pattern processes can \emph{call} themselves. This creates a
recursive loop. The state variables of the loop are listed in the process
parameter list after the \texttt{@} sign. If a process calls itself to recurse
only the new state variable values have to be passed in the call.

Sometimes it is not possible or necessary to use state variables to keep the
state during multiple loops of a process, for example when allocating an object
that does not change, but needs to persist during the various cycles of the
loop. In this case it is possible to use the \texttt{static} storage specifier
to make the values of variables persist after a process has recursed, as shown
in example \autoref{fig:csp:static}. To initialize such a value, or to only
execute some code once, the programmer can create a label called
\texttt{process\_init} followed by a (compound) statement.

\begin{figure}[H]
  \inputminted{c}{code/csp/static.mc}
  \caption{Usage of the \texttt{static} specifier in a process.}
  \label{fig:csp:static}
\end{figure}


%\begin{itemize}
%  \item Process description, process state.
%\end{itemize}

\section{Running processes in Parallel: \texttt{par}}
\label{sec:concurrency:par}

The \texttt{par} statement runs the processes concurrently (see example in
\autoref{fig:csp:drone-init}). The processes are listed inside the curly
brackets. The programmer can pass arguments to the processes, similarly to
normal function calls. Arguments can be pointers to channels. If a process
has state parameters the programmer needs to pass the default state values. The
arguments and default state values are separated by the \texttt{@} sign.

The \texttt{par} statement is a blocking statement. Only when all processes are
finished the execution continues. Because of this it is not necessary to join
the processes (threads) at a later point.

%\begin{itemize}
%  \item Invoking processes with \texttt{par} (from normal functions or other
%    processes)
%\end{itemize}

\section{Channels for communication between processes}

Communication between processes happens over channels. Channels are the wires
between the processes passing values to each other.

A channel is declared as a normal variable with a \texttt{chan<T>} type. The
type is parameterized, so only values of \texttt{T} can be read from or written
to the channel. An example would be \texttt{chan<int> ints;} for declaring a
channel of integers called \texttt{ints}.

Writing to a channel is denoted as \texttt{channel~!~expression}. This is a
binary expression with on the left side a reference to a channel variable and
on the right side an expression of the same type of the channel. By default the
expression blocks until another process can read the value from the channel.
However writing to a buffered channel does not block. The expression does not
have a result value.

Writes to a channel are strictly by value, a reader can modify the value at the
output but that will not change the value at the input. The programmer can
still pass a pointer to a channel, but as with passing a pointer to a function
the programmer has to understand that the consumer may change the value of the
object the pointer is pointing to. This semantic choice has two reasons:
firstly it is similar to C function calls, where the arguments are copied into
function call; secondly as with function calls it is still possible to pass a
pointer if that is necessary. To illustrate, in \autoref{fig:csp:drone-rotors}
the \texttt{rotors} process could modify one of the fields of the
\texttt{settings} variable but, as it is passed by value, the original
\texttt{settings} variable in the \texttt{controller} process has not changed.

The expression \texttt{channel~?~variable-reference} reads a value from the
channel. The left hand side of the expression is a reference to a channel and
the right hand side is a reference to a variable that has the same type as the
channel. The expression blocks until another process writes to the channel. The
value that the other process writes to the channel is then assigned to the
variable reference. The result of this expression is the received value:
\texttt{channel~?~var~==~var}.

The syntax using the \texttt{?} and \texttt{!} operators is taken from the
original CSP paper \cite{hoare1978communicating} and subsequent languages.

%\begin{itemize}
%  \item Channel declaration
%  \item Write to a Channel
%  \item Read from a Channel statement and expression
%  \item Pass-by-value
%  \item Blocking
%\end{itemize}

\section{Buffered Channels}

In embedded software, interrupt service routines (ISR) are triggered when
something happens. Interrupts can fire at any point in time and ISRs are
typically small. They should not block. By default, writing to a channel
blocks, that is not desired in ISRs. Adding a \texttt{buffered} storage
specifier to the declaration of a channel makes the channel non-blocking.
\texttt{buffered~chan<int>} creates a buffered channel of \texttt{int}s with
a default buffer size of \texttt{1}. In general \texttt{buffered[N]~chan<T>}
creates a channel with values of type \texttt{T} and a buffer size of
\texttt{N}. If the reader of the channel is too slow, the oldest value is
overwritten, so it is still the task of the programmer to find an appropriate
buffer size. If the buffer is empty, reading from the channel still blocks.
\autoref{fig:csp:drone-isr} already showed an example how it can be used.

%\begin{itemize}
%  \item Example with an Interrupt Service Routine
%  \item Buffered channels
%  \item non-blocking writes
%\end{itemize}

\section{Selection of a channel: \texttt{alts}}

In the example of \autoref{fig:csp:drone-rotors} all the setpoints, like roll
or speed are in one structure \texttt{SetPoints}. Instead the programmer could
choose to create separate channels for each setpoint, one for the speed, one
for yaw, one for pitch, and one for roll.

\begin{figure}[H]
  \inputminted{c}{code/csp/alts.mc}
  \caption{A process where the \texttt{alts} statement selects a value from
    one of the channels.}
  \label{fig:csp:drone-alts}
\end{figure}

\autoref{fig:csp:drone-alts} shows the controller process rewritten to use
multiple channels. The \texttt{alts} statement can be used to do something when
any of the channels receives a value. If one of the channels, \texttt{speed},
\texttt{roll}, \texttt{pitch}, or \texttt{yaw}, can be read, a value is read
from the channel and the statement after the colon is executed.

The \texttt{alts} statement looks similar to a \texttt{switch} statement but
the cases each have a channel. A \texttt{case} has the form of
\texttt{case~<guard>~<communication>:~<statement>}. The \texttt{guard} part
is optional. If it is given, inside square brackets, the expression should not
be \texttt{0} for the \texttt{case} to be selected. The \texttt{communication}
part can have two forms: just a reference to a channel or a channel read
expression. Finally if there is no guard or the guard is \emph{true}, and
the channel is ready to communicate, the \texttt{statement} is executed.

The first channel that is listed in the cases and is ready for input will be
selected. If it has a read expression, the read expression will assign the
value from the channel to the variable. Then the statement will be executed.

The \texttt{alts} statement will block until one of the cases is executed.

\section{Verification}
\label{sec:concurrency:verification}

An advantage of CSP is that formal verification tools exist to verify the
behavior of a program. One tool is PAT~\cite{SunLDP09}, which can simulate
and check for certain properties, such as deadlock freeness and reachability.

Models for the PAT program are written in CSP\#, their CSP language to model
systems. The definitions of processes and channels in the concurrency
extension of MetaC can be translated to CSP\#.  This makes it possible to
analyze a generated model of the actual implemented code.

\begin{figure}[H]
  \inputminted[fontsize=\footnotesize,linenos]{text}{code/csp/drone_motor.csp}
  \caption{The generated CSP\# model, slightly adjusted for readability.}
  \label{fig:csp:verification}
\end{figure}

\autoref{fig:csp:verification} shows a generated CSP\# model. At the top some
constants and the used channels are defined. As channels in CSP\# are defined
globally, the channel references passed into the arguments at \texttt{par}
statements are traced through the processes. The number after the channel name
defines the buffer size.

Then, from line 8, the processes are declared. All irrelevant information from
the MetaC code is filtered with only the communication as result. In CSP\#
reading from and writing to a channel has almost the same syntax as the MetaC
version. The \texttt{rotors} process reads a value from the
\texttt{chan\_rotors} channel after which in the final step is recursively
calling the process to accept a new value again.

In the \texttt{receive\_command} function the state variable,
\texttt{standby}, is translated to a CPS\# parameter. It reads two values
from the \texttt{chan\_rs232\_channel} for the command type and the value.
The \texttt{if} statement is translated as well in the CSP\# \texttt{if} on
line 15.  This determines whether it writes some value to the setpoint
channel and updates the standby state (line 16) or writes a value to the
setpoint channel and recurses with the current standby value (line 18).

The \texttt{ISR} process constantly writes a command to disable standby by
writing two values to the \texttt{chan\_rs232\_channel} channel. \texttt{Skip}
is a process that terminates directly, and is only used to create valid CSP\#
syntax.

On line 24 the \texttt{PAR\_0} process is created. This corresponds to
the \texttt{par} statement from \autoref{fig:csp:drone-init}. The last line is
a default assertion to check whether the system is deadlock free.

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{pics/drone-standby.png}
  \caption{The states of the simulated CSP\# model.}
  \label{fig:csp:simulation}
\end{figure}

When simulating the \texttt{PAR\_0} process with PAT the resulting image looks
like \autoref{fig:csp:simulation}. Each square represents a different state of
the system and the arrows are events that lead to a new state of the system.

What can be seen in this figure is that first the \texttt{STANDBY\_CMD} value
is written to the RS232 channel. At this point two events can happen in two
different orders: either the value is directly read by the
\texttt{receive\_command} process, or the second value is written to the RS232
channel. This is possible because the processes run concurrently and the RS232
channel is buffered. After either of the events happened, the other event is
executed as well.

After these two events, which happened in two different orders, the second
value from the RS232 channel is read. The \texttt{current\_command} is
substituted by \texttt{STANDBY\_CMD} in the \texttt{if} (line 15). Then a value
is written to and read from the setpoint channel (lines 16 and 10), so the
controller can write another value to the rotors channel (line 11) that is read
by the rotors process (line~8).

\paragraph{Modeling constraints}

As C code can be mixed and matched with the CSP constructs, special attention
is necessary by the programmer to generate a useful model.

\begin{itemize}
  \item Values written over a channel are assumed to be integers. Boolean and
    integers are the only data types PAT supports by default. Other types work
    fine, but are not considered in the model.
  \item The state of a model can be passed as \emph{state parameters}. These
    parameters are defined as \texttt{process name(t1 param1, t2 param2 @ s1
      state1, s2 state2)}. The state variables will be in the model. Changing
    the state can be done by calling the process as \texttt{name(new\_state1,
      new\_state2)} at the end of the process. The process will be executed
    again with the new state.
  \item The test expression of \texttt{if} statements can only depend on state
    variables or simple arithmetic expressions. Only then the model can make a
    deterministic choice, otherwise the model will be nondeterministic, meaning
    the statement could be executed or not. Consequently, the model will be
    able to reach certain states, that might falsify the properties the
    programmer likes to test, while in practice those could not happen.
  \item The same constraint holds for guard expressions of \texttt{alts} cases.
  \item Communication should not be done in C loops, instead recursive
    calls to execute the process again must be used.
  \item Other statements, that are not relevant or known to the model are,
    ignored.
  \item Model checking only works with a finite state space. So a recursive
    call with a continuously increasing counter will not work, as there will be
    a infinite number of states which a model checker (usually) cannot handle.
\end{itemize}

Currently the Concurrency MetaC extension does not warn the programmer if one
of these constraints are violated. Adding a warning to an \texttt{alts} guard
when variables other than state variables or constants are used would be an
example.  Preventing false positives would be the challenge here in order not
to irritate the programmer.

%\begin{itemize}
%  \item What can be verified: deadlock, traces
%  \item Small example to demonstrate verification
%  \item Process Analysis Toolkit (PAT) and the CSP\# modelling language
%\end{itemize}

\section{Implementation and C Runtime}

The concurrency extension adds special syntax for defining processes as
declarations, just like functions and variables. The syntax for channel
variables is defined as a \emph{type specifier}, just like the default types
such as \texttt{int} or \texttt{float}. The read and write expressions syntax
is defined as expressions, while the \texttt{par} and \texttt{alts} are defined
as statements, just like \texttt{if}.

The only new name bindings are for referencing processes, for example from the
\texttt{par} statement. However there are two special cases where the name
binding language was not sufficient and the Stratego name binding API had to
be used. Firstly, the processes need to be scoped for the current file. This is
the same problem as described for state machines in
\Cref{sec:statemachine:codegen}. A strategy
\texttt{get-program-scope-nabl-namespace} is also defined for processes so
BaseC picks up the process namespace if the file contains processes.
Consequently the processes are scoped in the enclosing file. Secondly, the
recursive function call to loop the process is a function call expression where
the function name is basically a variable reference. In this case the reference
should refer to the process. A similar problem was solved in
\Cref{sec:bitfields:implementation}, where a Stratego strategy tries to find
all possible namespaces to resolve the reference by building up a list by
calling the strategy \texttt{define-variable-ns}, which can be declared at any
place of any extension. The result is that Spoofax tries to look for variables,
functions and processes.

The types of the write and read expressions are found by looking up the type
of the variable reference. If the type of the variable is not a channel the
type system will generate an error. For the write expression the type of the
value that should be written to the channel is also checked, so it will not be
possible to write a strange type into a channel. For reading values from a
channel the type of the variable the value should be assigned too is also
checked against the channel type.

The generated C code uses a library called \emph{libcsp}~\cite{rick2000beton}.
It is a library that provides CSP features as a C API.\ This resembles the
syntax of this extension, so code generation becomes a bit easier: all the
complexity is inside the library, so the code generation only has to generate
the variable parts. Internally the library uses POXIS threads. Using a library
means adding a dependency, and the API has to be included. When the code
generation detects a process is declared in a file, a C
\texttt{\#include~<csp.h>} and \texttt{\#include~<process.h>} are added to the
top of the generated file.

\begin{figure}[H]
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/csp/codegen-process.mc}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \inputminted[fontsize=\footnotesize]{c}{code/csp/codegen-process-result.c}
  \end{minipage}
  \caption{Compilation scheme of a process.}
  \label{fig:csp:comp-scheme-process}
\end{figure}

\autoref{fig:csp:comp-scheme-process} shows how a process is transformed into
C. This function has one \texttt{Process} parameter that contains all process
information. This includes a pointer to the process arguments, that is declared
above the function.  When the process calls itself somewhere, a label is
created. This label is used with a \texttt{goto} statement at the location of
the call. Just before the call the arguments are assigned to the corresponding
variables.

\begin{figure}[H]
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/csp/codegen-read.mc}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/csp/codegen-read-result.c}
  \end{minipage}
  \caption{Compilation scheme of a channel read.}
  \label{fig:csp:comp-scheme-read}
\end{figure}

\autoref{fig:csp:comp-scheme-read} shows how a channel read is translated into
C code. The C code calls a libcsp library function \texttt{CSP\_chanInCopy}
with the channel, a pointer to the destination variable, and the number of
bytes that need to be copied from the channel to the variable: the size of the
channel type. The entire expression is a comma expression, with the last item a
variable reference to the destination variable, this leads that the result of
the entire expression is the value from the channel.

\begin{figure}[H]
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/csp/codegen-write.mc}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/csp/codegen-write-result.c}
  \end{minipage}
  \caption{Compilation scheme of a channel write.}
  \label{fig:csp:comp-scheme-write}
\end{figure}

An example of how a channel write is translated is shown in
\autoref{fig:csp:comp-scheme-write}. If the channel type is a 32 bit integer
the \texttt{CSP\_chanOutInt32} library function is used, where there are also
variations for 8, 16 and 64 bits. For other value types the generic
\texttt{CSP\_chanOutCopy} is used. The passed arguments are the variable
reference to the channel object, a pointer to a variable containing the value
to write to the channel and the number of bytes of the value. If the value
expression of the write is just a variable expression, that variable reference
can be passed as second argument, however if it is a more complex expression
the expression needs to be saved in a temporary variable first, before a
pointer to that temporary variable can be passed as argument.

\begin{figure}[H]
  \begin{minipage}{0.35\textwidth}
    \inputminted{c}{code/csp/codegen-par.mc}
  \end{minipage}
  \begin{minipage}{0.65\textwidth}
    \inputminted[fontsize=\footnotesize]{c}{code/csp/codegen-par-result.c}
  \end{minipage}
  \caption{Compilation scheme running processes with \texttt{par}.}
  \label{fig:csp:comp-scheme-par}
\end{figure}

The \texttt{par} statement runs processes concurrently.
\autoref{fig:csp:comp-scheme-par} shows an example how the code is transformed.
The \texttt{ProcPar} library function is the function to spawn and join the
threads. Each argument is a pointer to a process object. At this location the
arguments passed to the process are assigned to a structure object, and the
process object is initialized using the \texttt{ProcInit} function, that also
assigns a pointer of the process function to the object.

If channels are passed as process arguments, some channel initialization code
is generated as well. As channels can be declared globally, where an initial
value can only be a constant expression, they should be initialized before they
are used. The channel is cleaned up after after it is used.

\begin{figure}[H]
  \begin{minipage}{0.4\textwidth}
    \inputminted{c}{code/csp/codegen-alts.mc}
  \end{minipage}
  \begin{minipage}{0.6\textwidth}
    \inputminted{c}{code/csp/codegen-alts-result.c}
  \end{minipage}
  \caption{Compilation scheme selecting channels with \texttt{alts}.}
  \label{fig:csp:comp-scheme-alts}
\end{figure}

\autoref{fig:csp:comp-scheme-alts} shows the code generation of \texttt{alts}
statements. The main function is the \texttt{CSP\_priAltSelect} which accepts
a list of channels. The function blocks until one of the channels is ready to
communicate. When guards are added to the cases the code generation becomes
more complex. For each guard a function that returns the guard expression and
a structure is generated. All the variables that are used by the guard are
stored in the structure. The function and structure with variables are assigned
to two different fields of the channel. If one of the channels might
communicate libcsp calls the guard function with the variables object and if
the function result is \texttt{true} the channel can be selected.

%\begin{itemize}
%  \item Brief description of the generated C code.
%\end{itemize}

%\section{Evaluation}
%TODO
