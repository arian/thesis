\chapter{State Machine}
\label{ch:state-machines}

Almost all embedded software contains some state-based
behavior~\cite{koopman2010better}. This chapter discusses a MetaC State Machine
extension that provides special syntax for expressing state machines in code.
Mbeddr provides a similar extension. The syntax is very similar, both can
generate diagrams of a state machine declaration.

\section{Background}

State Machines model the behavior of a system with a finite number of
\emph{states}. A State Machine can only be in one state at the time. An
\emph{event} can trigger a transition to move to another state.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.7\textwidth]{pics/drone-simple.pdf}
  \caption{A State Machine graph of a simple Drone State Machine.}
  \label{fig:statemachine:drone-simple-graph}
\end{figure}

An example of a system where a state machine can be used is a drone. A state
machine diagram is shown in \autoref{fig:statemachine:drone-simple-graph} The
drone has multiple operational \emph{states}: off, on, calibrating and flying.
\emph{Events} are the inputs of the system from the outside, for example from
the person controlling the drone. Some events for this system are pressing the
on-off or calibrate buttons and new flight info (speed, rotations). An event
can trigger a \emph{transition}, the calibrate event will trigger a transition
from the on state to the calibrate state.

Traditionally a state machine is implemented in C with \texttt{switch}
statements. With growing complexity this might lead to deeply nested
conditional statements that are difficult to understand or test.

The state machine extension provides new syntax to define a state machine with
states and transitions between states.  The extension provides a clear
framework to structure the state machine code. The advantage compared to
runtime solutions, such as libraries, is that it enables more possibilities for
static analysis.  Additionally there is support for generating state chart
diagrams.

%, but the implementations are
%very different as the MetaC state machine extension is a text based extension
%while the mbeddr version uses MPS.

%\begin{itemize}
%  \item Motivation, what kind of applications would it serve. What are
%    additional benefits over a runtime library: stricter syntax, generate
%    FSM graphs, model checking.
%  \item Extension is interesting because mbeddr has a state machine extension
%    as well, with similar goal and solution.
%  \item Simple example, definition of a state machine, initial state, states,
%    events and event triggered transitions, epsilon transitions and transition
%    effects.
%\end{itemize}

\section{Specification}

%\begin{itemize}
%  \item Explanation of the example
%  \item More complex example, including guards, entry actions, generated graph,
%    usage of the state machine
%\end{itemize}

\autoref{fig:statemachine:drone} shows an example of the syntax of the state
machine extension. It shows a state machine describing a drone. The idea is to
have a similar construct as structures or functions, but for state machines.
Inside the construct the states and events are listed and in each state the
transitions are listed.

\begin{figure}[H]
  \inputminted[fontsize=\footnotesize]{c}{code/statemachine/drone.mc}
  \caption{A simplistic drone with four states and for events.}
  \label{fig:statemachine:drone}
\end{figure}

The initial state is the \texttt{off} state. The \texttt{onoff} event will
trigger the transition to the \texttt{on} state. The \texttt{on} state has
three transitions, back to \texttt{off}, to the \texttt{calibrate} state or to
the \texttt{fly} state, which is only possible when the local variable
\texttt{calibrated} equals \texttt{1}. If the \texttt{calibrate} event triggers
the transition to \texttt{calibrate} the new state of the state machine will be
the \texttt{calibrate} state. The \texttt{entry} block sets the local variable
\texttt{calibrated} to \texttt{1} to indicate it has calibrated the drone. This
state has only one transition, without an event, which will be executed
immediately after the entry block is done, and will transition back to the
\texttt{on} state. Now the \texttt{calibrated} variable equals \texttt{1}, the
transition to \texttt{flying} can be taken, but while taking the transition,
the transition action is executed, which will assign the \texttt{input\_speed}
parameter to the \texttt{speed} variable.  Finally the \texttt{flying} state
will be exited when the \texttt{land} event is triggered.

The following paragraphs will explain the different elements in more detail.

\paragraph{\texttt{statemachine} declaration}~\\
The \texttt{statemachine} keyword declares a new state machine type. After the
keyword follows a valid identifier indicating the name of the state machine.
The initial state, events, variables and states are declared inside the
following accolades. The programmer can declare the initial state with the
\texttt{init} keyword, name of the initial state and a semicolon. The initial
state declaration must come first, then events and variables can be defined
without a particular order and finally the states can be defined.

\paragraph{\texttt{event} declaration}~\\
An event is the input of the state machine by the user of the state machine.
Events can be declared inside the \texttt{statemachine} block by the
\texttt{event} keyword followed by a identifier which is the event name. The
\texttt{event} keyword is necessary to distinguish from function declarations
as \texttt{int foo();}. The following parenthesis indicates the event
parameters in a similar way as function parameters: a parameter type with a
parameter name, and multiple parameters separated by commas. The user of the
state machine can pass values into the state machine, for example the new
flying speed of the drone.

\paragraph{\texttt{state} declaration}~\\
Each state the state machine can have is declared with the \texttt{state}
keyword. The keyword is followed by the name of the state, which should be a
valid identifier. The content of the state is surrounded by accolades after the
name of the state. Transitions to other states are declared inside the
accolades, as well as an \emph{entry action}. An entry action is a code block
(compound statement) that is executed when the state machine enters this state.

\paragraph{\texttt{on} declaration for transitions}~\\
Inside the accolades of the state a programmer can define the transitions from
this state to another state. After the \texttt{on} keyword there are three
optional items: an event name, a \emph{guard expression} and a
\texttt{transition action}. The transition is ended with an arrow, \texttt{->},
the name of the new state and a semicolon.

The event name is optional. If an event name is given, the event parameters do
not need to be repeated, as they are defined at the top of the state machine
already. Multiple transitions from different states can be triggered by the
same event, which would mean duplication of the parameter list or conflicting
parameter types at the different transitions. Therefore the event parameters
are defined once and are referred to by only the event name.

A special type of transition is when the transition has no event name. This is
called an epsilon transition. This transition is taken directly after executing
the \texttt{entry} block, if the guard expression is \texttt{true}.

A guard expression is a boolean expression between square brackets. Only if
this expression evaluates to \texttt{true} the transition is taken. The event
parameters can be used inside this expression. Using guard expressions it is
possible that the same event triggers another transition. The transitions are
executed sequentially, so if multiple expressions are true for the same event,
the first transition is taken.

Transition actions execute some code statement when the state machine executes
a transition to a new state. This can be useful to set some variable or to show
something on a display. The transition action is denoted by a forward slash and
a C statement. This denotation is taken from UML state machines. The parameters
of the event can be used inside this statement.

\vspace{1\baselineskip}

\noindent
\autoref{fig:statemachine:drone_caller} shows how the
state machine of \autoref{fig:statemachine:drone} can be used.

\begin{figure}[H]
  \inputminted{c}{code/statemachine/drone_caller.mc}
  \caption{Syntax for using the Drone State Machine.}
  \label{fig:statemachine:drone_caller}
\end{figure}

In \autoref{fig:statemachine:drone_caller} an instance of the state machine
is created by declaring a new variable of the \texttt{statemachine Drone} type.
Events can be triggered by the \texttt{<|} operator, while the state machine
variables can be read from the state machine by using the \texttt{|>} operator.

\paragraph{State machine variable declaration}~\\
The programmer can use the state machine type in a variable declaration using
\texttt{statemachine} with the name of the state machine as variable type. This
initializes the state machine as well.

\paragraph{Trigger state machine events}~\\
The state machine does not change without events from outside. The programmer
can trigger events with the \texttt{state\_machine<|event(parameters)}
expression. The state machine variable is on the left side of the expression
and the event name is on the right side, followed by the event arguments. The
expression can be read as: input this event with parameters into the state
machine.

\paragraph{Accessing state machine variables}~\\
The variables that are declared inside the state machine can be accessed
outside the state machine as well using the \texttt{state\_machine|>variable}.
All variables of the state machine are visible, though accessing variables
should be done responsibly by the programmer, and a future \texttt{private} or
\texttt{static} keyword could enforce that.

\section{Implementation and generated code}

\subsection*{Syntax and Analysis}

The syntax of state machines is defined using SFD3 as a new declaration,
meaning that everywhere where declarations can be declared in a C program a
state machine type can be declared as well. Declaring a state machine variable
is defined as a declaration too.

Triggering state machine events and accessing state machine variables are
defined as post-fix expressions, like the \texttt{a++} expression. Expressions
have a certain precedence or priority; the \texttt{*} operator binds stronger
than the \texttt{+} operator. In SDF3 the context-free priorities defines the
relative priorities between productions to avoid grammar ambiguities. With the
priorities it is possible to use the same \texttt{Expr} sort declarations for
all types of expressions. However, to follow the C grammar specification BaseC
uses different sorts for the different types of expressions: binary, unary,
post-fix, etc. The state machine expressions are added as post-fix expressions
where the left-hand side can only contain other post-fix expressions. This
avoids ambiguities. However extensions could add other new expressions, for
example new binary operators that result in grammar ambiguities, as is done in
the fictive example in~\autoref{fig:statemachines:amb-grammar}. The ambiguity
is solved by the context-free priorities, but if another extension adds another
operator the context-free priorities need to define the relative priorities
between the two operators. Other extensions do not know about each other, so
defining the context-free priorities would need to be done in the main MetaC
module that includes all extensions.

\begin{figure}[H]
\begin{verbatim}
context-free syntax
  AExpr.At = [[AExpr] @ [AExpr]] {left}
context-free priorities
  { UnaryExpr.Negate }
  > { left: AExpr.At }
  > { AExpr.Add }
\end{verbatim}
  \caption{SDF3 Grammar with fictive \texttt{@} operator results in an
    ambiguous grammar.}
  \label{fig:statemachines:amb-grammar}
\end{figure}

The state machine expressions also have a different syntax than one might
expect. The member expression, \texttt{a.b}, using the dot, might be more
logical for new users of MetaC. This would require analysis that is currently
not supported by the Spoofax name binding language. Files from BaseC and files
from extensions should be able to contribute name bindings for the field
expression depending on the type of the left-hand side. Separate syntax will
parse to another AST node so a separate name binding rule can handle the name
binding of this node.

In BaseC the top AST node, \texttt{Program()}, scopes the variable and function
namespaces. Variables and functions are known in BaseC. The state machine name
binding definitions define the namespace for state machine, which BaseC has no
knowledge of, so BaseC cannot scope the state machine namespace. This would
result in an unwanted \emph{duplicate declaration} error when defining a state
machine of name \emph{x} in two separate files.

The solution for the scoping problem is to define the \texttt{nabl-get-scope}
strategy in Stratego in BaseC using the Spoofax Stratego API directly rather
than in the name binding language.  Instead of returning a list of name binding
namespaces directly, it collects the results of a custom
\texttt{get-program-scope-nabl-namespace} on the \texttt{Program()} AST node.
This \texttt{get-program-scope-nabl-namespace} can be declared in the modules
of any extension as Stratego composes multiple definitions of this strategy
with the choice, \texttt{<+}, operator, where the result of the first
succeeding strategy is returned. Now the state machine module can define the
Stratego rule using the stratego code shown in
\autoref{fig:statemachines:nabl-scope}. The \texttt{nabl-get-scope} executes a
search, and because of the \texttt{get-program-scope-nabl-namespace} definition
in the state machine module it succeeds and returns the name binding state
machine namespace, after which it is concatenated with the list of namespaces
of variables and functions which is the eventual result of
\texttt{nabl-get-scope}.

\begin{figure}[H]
\begin{verbatim}
get-program-scope-nabl-namespace:
  StateMachine(_, _, _, _, _) -> NablNsStateMachine()
\end{verbatim}
  \caption{Stratego rule to define the scoping rule of the state machines.}
  \label{fig:statemachines:nabl-scope}
\end{figure}

\subsection*{C Code Generation}
\label{sec:statemachine:codegen}

The generated C code consists of four elements: a structure with data
containing the current state and state machine variables, a transition function
that updates the state machine state when taking a transition and executes the
\emph{entry} code block, an \emph{exec} function that is called when an event
is executed, which may trigger transitions and execute transition actions.
Finally an \emph{init} function that initializes the state machine, by setting
the initial values of the state machine variables and executing the entry code
of the initial state.

\begin{figure}[H]
  \begin{minipage}{0.5\textwidth}
    \inputminted[fontsize=\footnotesize]{c}{code/statemachine/state-exec.mc}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \inputminted[fontsize=\footnotesize]{c}{code/statemachine/state-exec-result.c}
  \end{minipage}
  \caption{Compilation scheme of state in the \texttt{switch} of the
    \texttt{exec} function.}
  \label{fig:statemachines:comp-scheme-state-exec}
\end{figure}

The most important function is the \emph{exec} function.  This function takes
the state machine object and an event number as arguments. A while loop makes
sure that epsilon transitions, transitions without an event, are automatically
taken: when this function executes and a transition is taken, it will reset the
event number and execute the entire function body again so possible epsilon
transitions are executed too. A \texttt{switch} statement is inside the while
loop. \autoref{fig:statemachines:comp-scheme-state-exec} shows how a state is
translated into a case statement in the \texttt{switch} statement. Depending on
the current state the transitions from that state are checked with \texttt{if}
statements. If the event number argument matches the transition, and the guard
expression is true, the transition is taken by executing the
\texttt{transition\_to\_state} function which modifies the state machine object
with the new state. Epsilon transitions do not have an associated event, so
only the guard expression, if available, has to evaluated to true.

The local variables inside the state machine can be referred to as normal
variables inside entry statements, transition actions and transition guards. In
the C code they are stored in the state machine structure object. Therefore the
variable expression should be changed from \texttt{local\_variable} to a
pointer member expression, \texttt{sm->local\_variable}. However not all
variables should be transformed, for example variables from a higher scope,
like global variables. A variable expression is transformed if the \emph{nabl
uri}, the location of the AST node, of the state machine is equal to the
parent scope of the variable declaration an identifier refers to.

When a state machine is initialized by declaring a state machine variable,
a variable for the state machine structure is created. This structure is passed
into the \emph{init} function, which initializes the local state machine
variables and executes the initial transition to the initial state.

\begin{figure}[H]
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/statemachine/trigger.mc}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \inputminted[fontsize=\footnotesize]{c}{code/statemachine/trigger-result.c}
  \end{minipage}
  \caption{Compilation scheme of initializing a state machine and triggering an
    event.}
  \label{fig:statemachines:comp-scheme-trigger}
\end{figure}

\autoref{fig:statemachines:comp-scheme-trigger} shows how a trigger expression
is translated into the \emph{exec} function.  Each event is numbered and will
be passed to the \emph{exec} function call together with a pointer to the state
machine structure and the event parameters. The parameters are stored in a
separate structure and a pointer to this structure is passed into the exec
function so the transition guards and actions can use these values. Inside the
exec function the values are assigned to variables from the arguments structure
so the variable references do not have to be transformed when the event
parameters are used.

%\begin{itemize}
%  \item Implementation details: for example specific code to make collecting
%    the scope namespaces modular.
%  \item Brief description of the generated C code.
%\end{itemize}

\section{Generating State Chart Graphs}

One of the advantages of creating an abstraction for state machines with new
syntax is that analyzing the code becomes easier. By explicitly declaring the
states and transitions it becomes not only very clear for the programmer, but
the compiler can generate other artifacts than just code, such as diagrams. The
programmer can press the \emph{generate diagram} button in the MetaC IDE to
automatically generate a diagram. This diagram can help programmers to analyze
and better understand the system to make sure everything is implemented as
intended.

\begin{figure}[H]
  \includegraphics[width=0.8\textwidth]{pics/drone.pdf}
  \caption{A generated graph from the Drone State Machine code.}
  \label{fig:statemachine:drone_graph}
\end{figure}

The \autoref{fig:statemachine:drone_graph} shows the graph generated from the
code of \autoref{fig:statemachine:drone}. Each circle depicts a state and the
arrows between the circles are the transitions. If a transition is triggered
by an event, the event name is shown as the arrow label. For epsilon
transitions the label is \texttt{<<e>>}. If the transition has a guard, the
expression is added to the label between square brackets. The initial state is
represented with a transition that is only connected with one side to this
state. Terminating states, without outgoing transitions, have a double circle
in the diagram.

To generate the diagram the AST of the state machine is transformed to the
DOT language~\cite{Gansner00anopen}. In the DOT language the states are listed
with the correct shape: a circle for a non-terminating state and a double
circle for a terminating state. Then all transitions between the states are
listed using the event name as label. Guard expressions use the pretty-print
Stratego strategy generated by Spoofax and are added to the transition label.

Finally the diagram is generated using the default DOT tools.

%\begin{itemize}
%  \item Show the graph of listing~\ref{fig:statemachine:drone}
%  \item Further explanation of generating graphs.
%\end{itemize}
%

