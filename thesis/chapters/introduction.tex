\chapter{Introduction}

%\section{Background of Programming Embedded Software}

Embedded Software is software that runs on a system that is dedicated for a
specific function. This system is embedded in a bigger, typically mechanical,
device. Embedded software appears in a wide range of devices, from cars,
avionics, factory controllers to smart home appliances. The software controls
what a device should do: the software in a thermostat will enable or disable
the heating depending on the configured temperature.

The C programming language is very suitable for writing Embedded Software. That
makes it the most-used language for Embedded Software. Around 80
percent of the Embedded Software is written in C or
C++~\cite{ebert2009embedded}.

Embedded Software has a couple of requirements that are not common in other
fields of software development. Firstly, in contrast with software for Desktops
or Servers, embedded software is often deeply tucked away in the device and has
a limited user interface. This makes it very important that the software is
robust as it will be costly to restart or upgrade the system. For example, in
the case of a serious bug in the software for cars, the manufacturer needs to
recall many cars to the garage costing millions of euros.

% TODO: http://www.theregister.co.uk/2015/07/15/toyota_recalls_625000_hybrids_over_enginekilling_software_glitch/

Secondly, since the software is made for specific purposes, and the devices
are often mass produced, the hardware is also specific for the device. To
reduce costs the hardware will only just meet the minimum necessary
requirements, such as CPU abilities,  power and available memory. The software
is closely tied to the hardware as it usually controls physical parts of the
system. The programmer has to keep this under consideration when developing the
software.

% And POWER requirements

Finally, a requirement for many Embedded Software is that it should be
predictable, meet real-time deadlines and be deterministic. A car airback
should be precisely on time, not too early and not too late, otherwise the
execution of the software is at fault causing serious damage.

The C programming language meets the requirements. It provides very good
support for writing efficient, low-level code. Because of the popularity of C
many platforms have very good tool support, such as compilers and debuggers.

Unfortunately using C has many disadvantages as well. First of all it is hard
to make good abstractions that clearly describe the problem at hand. Examples
include data flow blocks, state machines or physical units. High-level
graphical modeling tools, such as Simulink, MetaEdit+ and LabView, provide a
nice graphical method where different types of components can be connected to
each other. However with these tools it is difficult to use lower level
hardware features, write C when that would be the best solution, or integrate
with legacy code.

Most other higher-level programming languages do not meet the requirements for
Embedded Software. A language such as Java has a non-deterministic virtual
machine with a Garbage Collector, which is very useful but can make the
software unsuitable for certain real-time tasks. Likewise, a popular language
as Python uses an interpreter which causes overhead, meaning that more powerful
hardware is required to do the same task.

Furthermore, while C is flexible and efficient, the language does not prevent
the programmer of making bugs, for example when using wrong integer sizes or
void function pointers. Tools such as static analyzers, for example
\emph{linters}, can detect various classes of defects while \emph{Valgrind} and
debuggers can help programmers find bugs when the software is run, but those
tools do not prevent making mistakes in the first place.

%Especially for Embedded Software that is connected to
%hardware peripherals, it can be difficult to isolate separate units for
%testing.

\section{The MetaC Approach: Extending C}

% AKA the what

Instead of taking a completely different approach by abandoning C altogether,
the solution followed in this thesis is to extend the C programming language
with domain specific language extensions. These extensions are sometimes
referred to as Domain Specific Languages (DSLs). The extensions should provide
new programming constructs that help Embedded Software programmers solve their
programming problems. If the programmer's problem involves a state machine, the
programmer can code the solution with a state machine extension with syntax to
create a state machine, the programmer does not need to create the state
machine with \texttt{switch} statements. The programmer does not have to think
anymore how to translate states and transitions of a state machine to C code,
but can define the states and transitions directly, which closes the gap
between the problem and the implementation code.

An advantage of using language extensions is that analyzing the program becomes
easier as the extension serves a specific purpose. A compiler could extract all
states and transitions out of a state machine and do specific analysis such as
finding terminal states.

Additionally, other artifacts can be generated from extensions. Depending on
the type of extension the compiler could generate diagrams or models that can
be analyzed by model checkers.

An already existing solution that uses language extensions is \emph{mbeddr}, an
extensible C-based language and IDE for embedded software
development~\cite{voelter2012mbeddr}. As will be explained later in
\Cref{ch:related-work} it uses projectional editing with the MPS workbench.
V\"olter et al.\ executed a case study that showed extensions can help the
development process, resulting in cleaner code, and reduced testing and
integration effort~\cite{voelter2015casestudy}.

In order to make it possible to see if extensions can help when implemented in
another context, this thesis describes \emph{MetaC} that implements different
extensions in a different way. Instead of the MPS language workbench Spoofax
will be used.  Spoofax is a textual Language Workbench for developing Domain
Specific Languages with IDE support~\cite{KatsVisser2010}.

\paragraph{Research Questions:} In this thesis the following questions will be
answered:

\begin{itemize}
  \item What could additional language extensions be? mbeddr already
    implements several extensions, so what are other extensions that could
    provide new abstractions and/or prevent bugs?
  \item How can C language extensions be implemented using Spoofax? mbeddr uses
    a projectional editing instead of a textual parser, which avoids grammar
    ambiguities when composing different extension grammars. Also how well
    does Spoofax provide support for building extensible languages?
\end{itemize}

%\begin{itemize}
%  \item Make writing C more robust
%  \item Make Specific Embedded Software tasks easier, as the language would be
%    closer to the problem, decrease complexity.
%  \item Make analyzing the program easier with static analysis / model checking
%  \item Chosen approach: Extend C with language extensions
%  \item Still C output, so many C benefits (tools, efficiency)
%\end{itemize}

\section{Approach using Spoofax}

% AKA the how

Spoofax is a language workbench for development of textual Domain Specific
Languages with IDE support~\cite{KatsVisser2010}. Spoofax makes it possible to
define a parser for a language using SDF3. The parser generates an Abstract
Syntax Tree (AST). This tree can be desugared to get a consistent tree that
is easier to work with during the next analysis and generation phases. With the
AST and by defining analysis rules Spoofax can resolve references and do type
checking using the Spoofax Name Binding and Type system meta languages.
Transformations can transform an AST to a modified AST, for example an AST with
AST nodes of extensions to an AST containing C AST nodes only. Stratego is the
transformation language used in Spoofax. Finally Spoofax can generate code from
the resulting AST.

A design goal of MetaC is that extensions are built in different modules that
are composed to the final MetaC language. That would make it possible to add,
modify, or remove extensions later, without interfering with the other
extensions.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.55\textwidth]{pics/modules.pdf}
  \caption{BaseC is the basis of MetaC, extensions are implemented separately
    on top of BaseC. MetaC composes everything to the final system.}
  \label{fig:intro:modules}
\end{figure}

Before it was possible to build extensions, the most essential module for the
basis C language was implemented as the \emph{BaseC} module. It defines the
standard C syntax and analysis, such as resolving variable references. Each
language extension is a separate module. The MetaC module imports BaseC and all
modules of extensions and composes them into the final language (see
\Cref{fig:intro:modules}).  Separate modules for extensions create a clear
separation so they can be developed independently according to the open-closed
principle: open for extension, closed for modification~\cite{martin1996open}.

Abstractions in the MetaC extensions should \emph{feel} familiar for C
programmers and integrate nicely into the C language. The rationale is that
existing C programmers should be able to quickly pick up the new syntax. This
means reusing existing C syntax, and designing new syntax that will be familiar
to C programmers.

The MetaC compiler will translate code using extensions to regular C code. This
output can then be used by a C compiler to compile for the target platform.
This makes building the MetaC compiler a lot simpler and makes extensions
easier to implement, while specialized C compilers are very good at compiling
to machine code.

\vspace{1\baselineskip}

\noindent
\paragraph{Outline} The structure of this thesis proceeds as follows. First
related work is discussed in \Cref{ch:related-work}. Then
chapters~\ref{ch:bitfields},~\ref{ch:state-machines},~\ref{ch:concurrency},
and~\ref{ch:error-handling} describe implemented extensions.
\Cref{ch:bitfields} is a small extension for working with bit manipulations.
\Cref{ch:state-machines} describes an extension for defining and using State
Machines, a similar extension mbeddr has. In \Cref{ch:concurrency} a
non-trivial concurrency extension is described with a C API as target. An error
handling extension is described in \Cref{ch:error-handling} that should make
error code returning functions safer. Finally \Cref{ch:conclusion} concludes
this thesis.

%\chapter{Running Example}
%\label{ch:running-example}
%
%This wouldn't be a separate chapter, but it would be nice if the examples all
%have a similar subject. I'm still working on something `real world' using
%QNX\@. That's a RTOS with an IDE and Virtual Machine so I can actually run it.
%
%\begin{itemize}
%  \item A quadcopter
%  \item Contains a FSM for different operational modes. Only certain
%    transitions are valid and only under certain conditions. When entering or
%    exiting a state it has to enable/disable some ISRs.
%  \item Some parts can be made as processes: Continuously reading the input,
%    can write to different channels, depending on the input or commands.
%    Another logger process need to log things. Finally a actuator process
%    should control the engines.
%  \item The quadcopter is controlled by a RS232 connection, which reads a
%    sequence of bytes. Sequences of bytes contain commands, but a sequence can
%    be corrupted, which needs to be handled gracefully.
%  \item Setting settings of peripherals are done by using bit-shifting.
%\end{itemize}
%
