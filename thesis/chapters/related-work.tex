\chapter{Related Work}
\label{ch:related-work}

MetaC is inspired by several other projects. In the following sections are
structured accordingly, one for each project.

\paragraph{mbeddr} A big inspiration of this work is \emph{mbeddr}. Mbeddr is
an extensible C-based language and IDE for embedded software
development~\cite{voelter2012mbeddr}. Embedded software typically has
reliability, safety, efficiency and real-time constraints as requirements.
The \emph{programming} approach using C or the \emph{modeling} approach using
tools that provide higher-level abstractions both have disadvantages.
\emph{Domain specific} languages (DSLs) are increasingly used in embedded
software, but most real-world systems cannot be described completely and
adequately with a single modeling tool or DSL.\ The mbeddr approach is a much
tighter integration between low-level C code and higher level abstractions
specific to embedded software.

The authors define five challenges: 1)~making abstractions without runtime
costs, 2)~make C safer, 3)~add program annotations, 4)~static checks and
verification, and 5)~process support. Mbeddr addresses these challenges with an
extensible version of the C language. Implemented extensions are
state-machines, components, modules, physical units, unit tests, requirement
traceability, and product line variability. The extensions are not limited to
these extensions, but the mbeddr system is designed to be extensible for other
extensions, which include project-specific extensions.

To provide an extensible language system as well as an IDE that is aware of the
extensions mbeddr uses the JetBrans MPS technology. MPS is a language
workbench, similar to Spoofax, for defining languages. Its  most important
characteristic is that it is a projectional editor. Instead of a parser parsing
text into an AST a projectional editor edits the AST directly. The projection
engine then generates a representation that is shown to the user. This
representation can be text, in case of editing C code in mbeddr, decision
tables, mathematical formulas, graphics, or something else the AST could
represent. Since no grammars are used, no syntactic ambiguities can result from
two independently developed languages. The challenge for projectional editing
is to make the editing experience convenient and productive. When the AST is
saved as a file the AST is stored as an XML file, that makes it harder to work
with tools such as version control.

In~\cite{voelter2015casestudy} Voelter et al.\ report on an industrial case
study using domain specific extensions for C with mbeddr. The goal is to
provide evidence to what extent language extensions supported by mbeddr are
useful for the development of embedded software. The authors test the following
aspects:

\begin{description}
  \item[Complexity] The developers of the case study project thought in terms
    of extensions and suggested additional extensions. The mbeddr component
    extension helps to structure the overall architecture. The used extensions
    facilitate in strong static checking, improve readability and help to avoid
    low-level mistakes.
  \item[Testing] The components extension of mbeddr used in the case study
    improved testability, leading to 80\% test coverage for core components,
    much higher than state-of-the-practice in industry. Custom extensions made
    hardware independent testing possible, so the system could be tested in a
    continuous integration process.
  \item[Overhead] The overhead of extensions can be categorized into three
    categories: 1)~without runtime footprint, 2)~similar footprint as manually
    written C code, and 3)~requires more sophisticated code structures. For the
    use case the performance overhead was low enough to meet the requirements.
    The components extension enables deployment of only the required
    functionality, minimizing the binary size.
  \item[Effort] The effort for integration and testing is lower than usual in
    embedded software. In total, including implementation, the effort is
    similar to what could have been achieved with C.
\end{description}

All these tested aspects seem to be positive. This can be explained because
mbeddr is specifically designed to achieve these benefits. The extensions
improve C incrementally, so the developer can use the appropriate language
features.

The developers of mbeddr have chosen MPS because of its support for modular
language extensions and flexible notation. To a degree those features are also
available in other language workbenches and they expect similar results when
building something like mbeddr in those workbenches.

To support the findings of this case study, the authors think additional
studies are necessary using mbeddr-based systems but also other extension-based
approaches in embedded software are necessary. The work on MetaC aims to
provide a starting point with an alternative implementation as a C-based
language with extensions for embedded software.

\paragraph{nesC} is a language based on C with a specific domain in mind:
embedded networked systems. The nesC language uses an event-driven programming
model for flexible concurrency and a component oriented-application design.
Using this model the compiler can perform static analysis to detect
race-conditions or perform aggressive function inlining. Components are
statically wired together and post tasks for the scheduler to execute. Tasks
are atomic, non-preemptable, for other tasks, which allows nesC to avoid
data-races.  However, tasks can be preempted by \emph{events} (usually
interrupts). Atomic code sections are a code blocks where interrupts are
disabled so further data-races can be avoided.

Contrary to other languages with a similar execution model, nesC is an
extension of C. This makes it possible to use the well known low-level features
necessary to access hardware and to interact with existing C code. NesC
provides help writing safer and more structured code.

The nesC extension to C is very specific. A similar extension could be built
using the mbeddr or MetaC approach. Then the mbeddr or MetaC toolset can be
used, and the extension could be used alongside other extensions.

\paragraph{Hume} is another Domain Specific Language approach for Real-Time
embedded systems that is not based on C but is an external
DSL~\cite{hammond2003hume}.\ The language is based on a combination of
$\lambda$-calculus and finite state machine notations with a syntax similar to
Haskell.  Its goal is to provide high level abstractions while maintaining
properties necessary for real-time systems, such as determinancy and
bounded time constraints. The main abstraction is boxes, that can be wired
together, like nesC components. The language is layered. A meta programming
layer simplifies the creation of boxes and wires, for example for different
types or repetition of boxes. Exceptions that are raised must be handled by the
surrounding box which is checked by static analysis.

\vspace{.5\baselineskip}
\noindent
Both nesC and Hume are domain specific languages for embedded software. Like
mbeddr and MetaC, nesC extends C, but only with one \emph{specific} extension,
while mbeddr and MetaC could potentially add the nesC extension as well.

Hume, and other DSLs such as Feldspar~\cite{axelsson2010feldspar}, have a
completely different syntax than C. This allows compilers to do more static
analysis to guarantee safety, or to do more optimizations. A drawback is the
interaction with legacy code that becomes harder to integrate or to migrate
from.

Mbeddr tries to be a generic solution for specific extensions to C. Its authors
have chosen the MPS language workbench to implement it, but that forces
developers to use the MPS IDE as well. Though mbeddr has shown that extensions
are a viable solution for embedded software development, with increased
abstractions in the code and reduced integration and testing effort. MetaC
takes these ideas but has an alternative, textual implementation with some
different extensions.

%\begin{itemize}
%  \item mbeddr: DSLs for C, using MPS
%  \item NesC: TinyOS specific extension for C
%  \item hume: A DSL for Real Time Embedded Systems, A Haskell-like language
%    that uses a much more radical method.
%\end{itemize}
