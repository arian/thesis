\chapter{Error Handling}
\label{ch:error-handling}

Error handling is a key component of a reliable software system. It allows the
system to detect errors and handle them correctly, for example recovering the
error or by signalling an appropriate error message. Despite its importance,
error handling is often the least well understood, documented and tested part
of a system~\cite{bruntink2006discovering}. C does not have explicit error
handling, instead typically programmers use a convention of
\emph{return~codes}: functions return a special value the caller of the
function has to check. The error handling extension adds special syntax for
error handling allowing the compiler to do static checks.

\section{Background}

The C convention of error handling is that functions return a special value,
for example \texttt{-1}, \texttt{NULL}, or another previously defined constant.
The programmer using this function has to check whether this special value was
returned by the function, and if so, handle the error appropriately. However
the compiler will not prevent the programmer from skipping the error check.

\begin{figure}[H]
  \inputminted{c}{code/errors/getchar.c}
  \caption{An example of a function, reading from an array, returning
    \texttt{-1}, to indicate that there are no new values.}
  \label{fig:err:getchar}
\end{figure}

\begin{figure}[H]
  \inputminted{c}{code/errors/process_input.c}
  \caption{How the \texttt{getchar} function is used and how the check for
    errors is typically done.}
  \label{fig:err:process_input}
\end{figure}

\Cref{fig:err:process_input} shows an example where input is being
processed. The \texttt{getchar} function of \Cref{fig:err:getchar} returns a
character code, or \texttt{-1} if the buffer it reads from is empty. The
\texttt{process\_input} gets two characters or prints a message if one of the
two characters is not a valid character. C code structured similarly lead to
the \emph{\#gotofail} security bug in Apple's iOS operating
system~\cite{deursen2014learning}.


More modern languages have error handling constructs: Java, amongst others,
has \texttt{try-catch} blocks where thrown exceptions are caught. In some
functional languages \texttt{Maybe} or \texttt{Either} objects are returned by
functions. In combination with pattern matching the programmer is forced by the
compiler to \emph{unpack} the object and handle all possible cases, including
the error case.

The error handling extension adds new syntax for handling errors. Using this
extension the programmer is forced to handle the error. Instead of propagating
errors up the call stack, a simpler variant is implemented here. Instead of
throwing an error that is caught and handled somewhere on the call stack, the
function returns an error object the user of the function must handle directly.
Firstly this forces the programmer to handle errors quickly and, secondly is
more practical to implement as transformations can be kept locally.

Although other modern languages have first-class error handling support, Mbeddr
does not have a similar language extension yet.

%\begin{itemize}
%  \item Motivation: a better contract between function definitions and
%    invocation for errors.
%  \item Interesting because not in mbeddr, fairly straight forward translation
%    to C.
%\end{itemize}

%\begin{listing}[H]
%  \inputminted{c}{code/err/gotofail.c}
%  \caption{C code error handling}
%  \label{lst:err:gotofail-c}
%\end{listing}

%\begin{listing}[H]
%  \inputminted{c}{code/err/gotofail.mc}
%  \caption{Error handling with MetaC}
%  \label{lst:err:gotofail-mc}
%\end{listing}

%\begin{listing}[H]
%  \inputminted{c}{code/err/mc_err_stdio.mc}
%  \caption{MetaC definition of \texttt{mc\_fopen}, would be a library file}
%  \label{lst:err:mc-err-stdio}
%\end{listing}

\section{Error Function Definition}

The error handling extension adds special syntax for function return types to
indicate that a function could return an error. Instead of a normal return type
\texttt{T}, for example \texttt{int}, the return type of a so-called
\emph{error function} is \texttt{MaybeError<T>} or \texttt{MaybeError<T,E>}.
\texttt{E} is the type of an optional error value the error function can
return.

\begin{figure}[H]
  \inputminted{c}{code/errors/getchar.mc}
  \caption{Modified \texttt{getchar} function using \texttt{MaybeError<int>}
    as return type.}
  \label{fig:err:getchar-mc}
\end{figure}

\Cref{fig:err:getchar-mc} uses the \texttt{MaybeError<int>} as return type.
Instead of returning \texttt{-1} a new error object \texttt{Error()} is
returned to indicate the function cannot read new values, otherwise the
function is identical. An important effect of this change is that the
\texttt{getchar} function cannot be used directly in expressions or assignments
anymore as it would not type-check: the programmer has to use the function
inside an \texttt{attempt-fail} statement as explained in
\Cref{sec:err:attemt-fail}.

If a function can fail due to multiple causes, a value can be passed
to the \texttt{Error()} object, for example \texttt{Error(BUFFER\_EMPTY)}.
For this variation the function return type should be of the form
\texttt{MaybeError<T,E>} where \texttt{E} is the return type of
\texttt{BUFFER\_EMPTY}, for instance \texttt{MaybeError<int,unsigned~char>}.
Later, where this function is used, the user can retrieve this value and handle
the error appropriately.

%\begin{itemize}
%  \item Definition of a function that can return a MaybeError$<$T$>$
%\end{itemize}

\section{\texttt{attempt-fail} statement: usage of error functions}
\label{sec:err:attemt-fail}

As using error functions directly will not type check, as \texttt{MaybeError}
is the return type, the programmer can use something special to use the
functions: the \texttt{attempt-fail} statement. This statement consists of an
\texttt{attempt} code block and one or multiple \texttt{fail} code blocks.

\begin{figure}[H]
  \inputminted{c}{code/errors/process_input.mc}
  \caption{Modified \texttt{process\_input} function with an
    \texttt{attempt-fail} to retrieve the values from \texttt{getchar} or
    handle errors appropriately.}
  \label{fig:err:process_input-mc}
\end{figure}

Inside the blocks the programmer can use normal C code. However inside the
\texttt{attempt} block an \emph{error variable declaration} can be used.
This is like a normal C variable declaration: a type specifier, name
identifier, and an initial value. The difference is that instead of a
\texttt{=} sign a \texttt{?=} sign is used, and that the initial value is a
function call to the error function. Also the variable type is \texttt{T} from
\texttt{MaybeError<T>} instead of \texttt{MaybeError}. If an error variable
declaration is used outside an attempt block, the compiler can parse the code,
so there will not be a syntax error, but during analysis the compiler will
generate an error as feedback to the programmer.

The variable declaration has a slightly different syntax to indicate that it is
not a normal variable declaration: it does more than only assigning the return
value to the variable. If the function call results in a failure, it will
not assign the variable with the result, but will jump to a fail block.

Inside fail blocks the programmer can handle the errors. The fail block in
\Cref{fig:err:process_input-mc} shows the generic fail block, which will be
used by default. Because there can be multiple error variable declarations
inside the attempt block, with multiple different error value types, the
different fail blocks can handle the different types of the error values.
The programmer can add a parameter to the fail block:
\texttt{fail~(E~e)~\string{\ldots\string}}, This block will handle
the error of a variable declaration that uses an error function where the
return type is \texttt{MaybeError<T,E>}. For example
\texttt{fail~(int~err)~\string{~printf(strerror(err));~\string}} will handle
the error of a function returning \texttt{MaybeError<char,int>}, as the
\texttt{int} error value matches the fail block parameter type. If no
corresponding fail block could be found, the compiler will generate an error
as feedback to the programmer.

\begin{figure}[H]
  \inputminted{c}{code/errors/multi-fails.mc}
  \caption{Different \texttt{fail} blocks that will handle different errors.}
  \label{fig:err:multi-fails-mc}
\end{figure}

\Cref{fig:err:multi-fails-mc} shows an example of two different functions
with different error value types: if the \texttt{push} call fails it will jump
to the fail block that has \texttt{enum full\_error} as parameter type. On the
other hand the \texttt{pop} function call would jump to the other fail block
with the \texttt{enum empty\_error} parameter type.

In the example of \Cref{fig:err:multi-fails-mc} the \texttt{p} variable is not
used. An improvement would be that this code is an intermediate, desugared,
form and the functions can be used directly in expressions, for example
\texttt{push(pop())}.

%\begin{itemize}
%  \item At the call-site, show how the function can be used inside an
%    attempt-fail block.
%  \item Using an error function outside an attempt-fail block will result in
%    an error.
%\end{itemize}
%
%\section{Handling multiple error types with multiple fail blocks}
%
%\begin{itemize}
%  \item MaybeError$<$T,E$>$ with an extra error type.
%  \item Multiple fail blocks, for different error types by the invocations in
%    the attempt block.
%\end{itemize}

\section{Implementation}

\paragraph{The syntax definitions} of the \texttt{attempt} statement adds a new
statement type. The error variable declaration is also defined as a statement.
If it would be declared as a normal declaration it would also be possible to
use it at the top level of a file, like a global variable, and that should not
be possible.

The \texttt{MaybeError} syntax is defined as a type specifier. That makes it
possible to use it at the return type position of a function declaration.

\paragraph{During the analysis} phase, the name binding declares the error
variable declaration AST nodes as normal C variables. The error value parameter
of a fail block is defined as variable only in the fail code block, that
scopes the variable.

The fail blocks themselves are also registered to the name binding index
engine, and if present, the parameter type is stored as property. These are
used by custom error handling Stratego \texttt{nabl-use-site} strategies that
are called by the analysis engine. When an error variable declaration is
traversed by this strategy an analysis task looks up the type of the function
call, and rewrites it to the type of the error value. Another task finds all
registered fail blocks of the attempt block the error variable declaration is
in. The stored parameter type property is then used to filter the found fail
blocks against the error value type of the function call in the error variable
declaration.  Finally another task looks up if there is a registered default
fail block (without a error value property). The result is either a fail block
with matching parameter type, the default fail block, or no result at all. If
there is no result a task is created to create a new error message informing
the programmer.

\paragraph{C Code generation} translates the special error handling constructs
to normal C code.

\begin{figure}[H]
  \begin{minipage}[t]{0.55\textwidth}
    \vspace{0.8cm}
    \inputminted{c}{code/errors/push.mc}
  \end{minipage}
  \begin{minipage}[t]{0.45\textwidth}
    \inputminted[fontsize=\small]{c}{code/errors/push-result.c}
  \end{minipage}
  \caption{Compilation scheme of an error function.}
  \label{fig:errors:scheme-function}
\end{figure}

\Cref{fig:errors:scheme-function} shows how an error function is transformed
to C code. When the code generator matches a function that returns a
\texttt{MaybeError}, the function will be transformed into a new union type.
The return type of the function will be changed to
\texttt{unsigned~char}. This return value will be either \texttt{1} or
\texttt{0} to indicate success or failure. A parameter with the type of the
union is prepended to the function parameter list. This union will hold the
return value if the function succeeds, if the function return type includes an
error value type the union holds the error value when the function fails.
Inside the function the expressions of \texttt{return}s are transformed too.
\texttt{return Error()} is transformed to \texttt{return 0}, \texttt{return
  Error(value)} is transformed to a comma expression, the first expression sets
the \texttt{error} field of the union object to the error value, and the last
expression is the \texttt{0} constant.  Other return values are considered
successful and are also replaced by a comma expression, but instead set the
\texttt{value} field and return the \texttt{1} constant.

\begin{figure}[H]
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/errors/attempt.mc}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \inputminted[fontsize=\small]{c}{code/errors/attempt-result.c}
  \end{minipage}
  \caption{Compilation scheme of an attempt statement.}
  \label{fig:errors:scheme-attempt}
\end{figure}

The \texttt{attempt} statements are transformed to variable declarations of the
fail block parameters with a unique name, a block statement containing the
contents of the attempt block, labeled block statements containing the contents
of the fail blocks and, a label at the end.

An error variable declaration is transformed to a variable declaration for the
union type that will hold either the success or error value. The function is
called by a subsequent \texttt{if} statement. The function returns \texttt{1}
or \texttt{0} so can be used directly in the \texttt{if} statement. The
function call result is negated, so the \texttt{true} branch of the \texttt{if}
statement is only executed if the result of the error function is an error.  In
the branch the error value from the union object is assigned to the previously
declared error parameter, if the function has an error value, and the program
jumps to the corresponding label of the fail block using a \texttt{goto}.  If
the function was successful the branch was not taken, so the next declaration
assigns the success value to a variable. At the end of the attempt block a
\texttt{goto} is necessary to jump to the end to ensure the statements of the
following fail blocks are not executed. Inside the fail blocks, variable
references to the parameter are transformed to use the unique variable name
given to the parameter. At the end of each fail block a \texttt{goto} ensures
that the program jumps to the end of the \texttt{attempt-fail} statement.

%\begin{itemize}
%  \item Implementation of fail-block resolution. Impossible using the default
%    NaBL meta-language.
%  \item Brief description of the generated C code.
%\end{itemize}

