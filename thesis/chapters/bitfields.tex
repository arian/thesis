\chapter{Bit Fields}
\label{ch:bitfields}

The first MetaC extension is for bit manipulations. Bit manipulation is a
technique often used when writing C programs, especially when working closely
on the hardware, as in Embedded Software. Getting the bit manipulations
correct the first time can be tricky to program. This extension should make it
easier for the programmer to work with bit fields.

A similar extension is not available in mbeddr. This extension is relatively
straightforward to implement, as reads and writes of Bit Fields can easily be
translated to the related bit manipulations.

\section{Background}

Bit manipulation is a way to read and write specific bits at a given memory
location. A memory location can be connected to peripherals, like sensors or
actuators. For example reading from a certain memory location can give the
current value of a sensor. Alternatively, writing values to a certain location
can configure peripherals. Bit manipulation can also be useful dealing with
boolean data in a memory efficient way as only one bit is needed for each
value.

\begin{figure}[h]
  \centering
  \includegraphics{pics/bits.pdf}
  \caption{Layout of register bits of an serial interface.}
  \label{fig:bitfields:layout}
\end{figure}

The principle of working with bits is that bits are ordered accordingly to a
given layout. Manipulating bits in C requires logical operators such as the
\emph{and}, \emph{or}, \emph{complement}, \emph{exclusive or} and \emph{left}
and \emph{right shift}. \autoref{fig:bitfields:layout} shows an example of the
layout of some serial interface (UART) registers.
\autoref{fig:bitfields:legacy} shows the traditional method how bits can be
manipulated using the existing operators in C.

\begin{figure}[h]
  \inputminted{c}{code/bitfields/legacy.c}
  \caption{Traditional method of manipulating bits in C.}
  \label{fig:bitfields:legacy}
\end{figure}

C programmers will understand the code in~\autoref{fig:bitfields:legacy} quite
easily, but unfortunately it is easy to make small mistakes that the programmer
will not notice directly. Muscle memory could cause writing the \emph{logical
and} (\texttt{\&\&}) operator instead of the \emph{bitwise and} (\texttt{\&})
operator. The compiler will happily compile the code because it does not
understand the intent of the programmer, for example when he or she wants to
test for a bit using the expression \texttt{flags \& INTERRUPT\_ENABLE}.

When working with groups of bits the operations become more complex since
multiple bits have to be modified. An operation such as ``set the middle two
bits to a value of 2'' can require some puzzling by the programmer to get
right.

Standard C structures can be used as a solution. When using structures, the
number of bits that need to be used can be specified. However the C programming
language specification only guarantees that the compiler must reserve this many
bits for the field. It does not guarantee that the bits are ordered accordingly
to a certain layout. If the compiler wants, it can put padding bits between
fields to make other operations faster. It could place a field at
the 8th bit instead of the 6th. Given this freedom for the compiler, this
method cannot be used when the layout of the bits is significant.

\begin{figure}[h]
  \inputminted{c}{code/bitfields/simple.mc}
  \caption{a bit fields extension example.}
  \label{fig:bitfields:simple}
\end{figure}

\autoref{fig:bitfields:simple} shows an example of the usage of the proposed
Bit Fields extension. First the layout of the Bit Fields are declared using the
\texttt{bitfields} keyword. This declaration contains the fields with their
names and the number of bits each field has. At another location the Bit Fields
declaration can be used by declaring a variable with the type as the name of
the Bit Fields declaration. In this example this is \texttt{UART}. Finally by
using the \emph{member operator}, \texttt{a.interrupt\_enabled}, the bits can
be assigned or read.

\section{Specification of Bit Fields Declarations}

The Bit Fields declaration is designed to look like C \texttt{struct} or
\texttt{union} declarations. A new Bit Fields declaration is defined by the
\texttt{bitfields} keyword. A \texttt{bitfields} declaration must have a valid
identifier as name. The declaration declares a new type of this name and is
used to refer to this Bit Fields declaration elsewhere in the program. After
the name a list of fields, the layout, follows, enclosed by braces. Each field
has a name as identifier, followed by a colon ``\texttt{:}'' and an integer to
indicate the number of bits the field can hold. Each field is ended with a
semicolon ``\texttt{;}''.

Field names have to be unique for each Bit Fields declaration. However two
different Bit Fields declarations can both have a field with the same name.

The positioning of the bits is laid out as shown in
\autoref{fig:bitfields:layout}. The bits are aligned to the right.
For \texttt{bitfields UART} from \autoref{fig:bitfields:simple} the last field,
\texttt{baud\_rate}, will be placed at the three right-most positions, followed
by the \texttt{parity} field, that will be placed at locations 3 and 4, the
\texttt{unused} field at 5 and 6, and finally \texttt{interrupt\_enabled} at
position 7.

\section{Bit Fields usage}

A variable can be declared with the Bit Fields name as variable type:
\texttt{bitfield\_type variable\_name;}. Optionally an initial value can be
given to the declaration. This value should be an integer literal. The variable
can also be declared as pointer, so a pointer to a number or even another
Bit Fields variable can be assigned to the variable.
\autoref{fig:bitfields:variables} shows a few different possible variations.

\begin{figure}[htp]
  \begin{minted}{c}
bitfields A { a: 2; b: 4; c: 2; };
void f() {
  A a; // without initializer
  A b = 0x1F; // arbitrary number
  A *c = &a; // pointer to another number or bitfield
}
  \end{minted}
  \caption{Examples of declaring Bit Fields variables.}
  \label{fig:bitfields:variables}
\end{figure}

Setting and getting values to and from Bit Fields is done using the same
operator as structures or unions already known in C with the member operator
``\texttt{.}''. The left hand side of the operator needs to refer to a Bit
Fields variable while the right hand side of the operator should match one of
the fields listed in the Bit Fields declaration of that variable.

If the member expression happens on the left side of an assignment expression,
it will be used as an assignment. The right hand side of the expression will
be written to the relevant bit positions the field refers to.

If the member expression happens elsewhere, it is used to retrieve the data.
The expression will evaluate to the value in the memory the bits of the field
of this expression refers to.

Bit Fields variables declared as pointer, \texttt{A *b;}, would need to
dereference the variable first, so \texttt{*b} refers to the Bit Fields, and
\texttt{(*b).field} would refer to some bits. Instead of writing
\texttt{(*b).field}, it is possible to use the \texttt{b->field} operator, just
like structures in C. \autoref{fig:bitfields:pointers} gives a complete example
of the usage of Bit Fields pointers.

\begin{figure}[h]
  \inputminted{c}{code/bitfields/pointer.mc}
  \caption{Using the \texttt{->} operator.}
  \label{fig:bitfields:pointers}
\end{figure}

\section{Implementation}
\label{sec:bitfields:implementation}

The only new syntax for Bit Fields is the Bit Fields declaration with the
names of the fields and the corresponding sizes.

During the desugaring phase, the start and end positions of each field are
determined. In the case of \autoref{fig:bitfields:layout} field
\texttt{baud\_rate} would have range $0\ldots2$, field \texttt{parity} range
$3\ldots4$ and so on. The desugared Bit Fields declaration will also contain
the total number of bits.

In the analysis phase the Bit Fields declaration defines itself as a new
\texttt{typedef} type, so type names  will refer to the Bit Fields
declarations. Each field defines itself and is scoped by the Bit Fields
declaration. The range and size of the field are stored as properties so they
can be used when the field is referenced.

\begin{figure}[h]
  \begin{verbatim}
Field(e, Identifier(field)):
  refers to Field field in Struct s
    where e has type Struct(Identifier(s))
  otherwise refers to Field field in Union s
    where e has type Union(Identifier(s))
  \end{verbatim}
  \caption{Spoofax Name Binding code for for resolving member expressions.}
  \label{fig:bitfields:fields-nabl}
\end{figure}

The fields are referenced by the default C member operators \texttt{a.b} and
\texttt{a->b}. Resolving fields depends on the type of the left side of the
expression. In BaseC it first looks if it is a structure, otherwise it will try
to use it as a union, see \autoref{fig:bitfields:fields-nabl}. This should be
altered to include Bit Fields.

\begin{figure}[h]
\begin{verbatim}
field-lookup = register-field-lookup(|
    "rewrite-struct-name",
    NablNsStruct(),
    NablNsField())
task-rewrite: ("rewrite-struct-name", Struct(Identifier(s))) -> s
\end{verbatim}
  \caption{Stratego code to register a new candidate.}
  \label{fig:bitfields:fields-register-candidate}
\end{figure}

To keep the implementation modular, BaseC should not include Bit Fields code,
thus the default Spoofax Name Binding language solution will not be good
enough. The BaseC name binding can use Stratego with the underlying name
binding API directly. At the \texttt{nabl-use-site} the adjusted BaseC name
binding code tries to collect all registered candidates in a modular fashion.
The essential differences between each candidate are the namespaces of the
enclosing construct, the field namespace, and a mapping from the type to the
type name, for example \texttt{Struct(X)~->~X}.

Registering candidates happens by creating a \texttt{field-lookup} strategy
that is called from the BaseC code with a list of already registered
candidates. This strategy uses \texttt{register-field-lookup} that was added to
the BaseC Stratego API.\ This strategy will fail if the candidate is already in
the list, otherwise it will succeed and return a new list with the extra
candidate. The result is that the BaseC strategy collects a list of methods
defined at different places how the field expression can be used.

\subsection{C Code Generation}

\begin{figure}[H]
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/bitfields/decl.mc}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/bitfields/decl-result.c}
  \end{minipage}
  \caption{Compilation scheme of a \texttt{bitfields} declaration.}
  \label{fig:bitfields:comp-scheme-bitfields}
\end{figure}

When the code analysis analysis is complete the C code can be generated. Bit
Fields declarations are transformed to \texttt{typedef} declarations, as shown
in \autoref{fig:bitfields:comp-scheme-bitfields}. The name of the declaration
is the same as the Bit Fields declaration name. This way the type can still be
used in other places like function parameters or cast expressions. The integer
type used for this \texttt{typedef} depends on the total number of bits in the
Bit Fields declaration. If the number of bits has a maximum of 8, the type is
\texttt{uint8\_t}, for 16 \texttt{uint16\_t}, for 32
\texttt{uint32\_t} and for 64 \texttt{uint64\_t}.

\begin{figure}[H]
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/bitfields/var-decl.mc}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \inputminted{c}{code/bitfields/var-decl-result.c}
  \end{minipage}
  \caption{Compilation scheme of a Bit Field variable declaration.}
  \label{fig:bitfields:comp-scheme-var-decl}
\end{figure}

\autoref{fig:bitfields:comp-scheme-var-decl} shows how a variable declarations
is transformed. If it has an initial value, that value is casted to the correct
type to prevent C compiler warnings. For example this allows overlaying 32 bit
integers with a Bit Fields declaration with only 8 bits. If the variable
declaration does not have an initial value, the declaration is changed so the
initial value of \texttt{0} is assigned. This should prevent confusions when
the C compiler does not do this.

To retrieve bits the number is shifted to the right first, and then masked to
filter only the relevant bits. For a field with the third and fourth bits, that
translates to \texttt{(a >> 2 \& 0x3)}, where \texttt{a} is the variable.

For setting the third and fourth bits
\texttt{a = (a \& $\sim$(0x3 << 2) \& 0xFF) | ((1 \& 0x3) << 2)} is used to set
them to value \texttt{1}. At the left-hand side of the logical-or expression
the bits are unset. The right-hand side makes sure the value will fit in the
two bits and shifts the value to the correct location. The result is ORed
and written to the variable \texttt{a}.

\section{Evaluation}

The Bit Fields extension can be a useful extension for programmers when working
with bits. The generated code is quite small, but to get the analysis and code
generation working some workarounds were necessary with the Spoofax
implementation. The \texttt{register-field-lookup} works so far because all
types are different (structures, unions, or bit fields) the order they are
registered is not relevant. If it would be relevant composing the field lookup
analysis would be harder because different extensions should not know about
each other, so MetaC would need to resolve the precedence.
