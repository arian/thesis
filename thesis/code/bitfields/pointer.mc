#include <assert.h>

bitfields A { x: 4; y: 4; };

int main() {
  int number = 0;
  A *b = &number;

  b->x = 1;
  assert(number == 0x10);

  b->y = 4;
  assert(number == 0x14);

  return 0;
}
