#define INTERRUPT_ENABLE (1 << 7)
#define PARITY_SHIFT 3
#define PARITY (3 << PARITY_SHIFT)
#define BAUD_RATE (7 << 0)
int main() {
  /* the memory to modify */
  unsigned int flags = 0;
  /* enable bit 7 */
  flags |= INTERRUPT_ENABLE;
  /* disable bit 7 */
  flags &= ~INTERRUPT_ENABLE;
  /* toggle bit 7 */
  flags ^= INTERRUPT_ENABLE;
  /* test the bit */
  if (flags & INTERRUPT_ENABLE) {
  }
  /* set multiple bits to a new value: 2 */
  flags &= ~PARITY;
  flags |= 2 << PARITY_SHIFT;
  return 0;
}
