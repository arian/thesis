#include <assert.h>

// define the layout of the fields
bitfields UART {
  // 'interrupt_enabled' is the name of the field
  interrupt_enabled: 1;
  unused: 2;
  parity: 2; // 2 is the number of bits
  baud_rate: 3;
};

int main() {
  // declare a variable of bitfields layout UART
  UART a;
  // set field 'interrupt_enabled' to 1
  a.interrupt_enabled = 1;
  // assert that the bit has changed
  assert((a & (1 << 7)) != 0);
  // field 'parity' with 2 bits becomes 2
  a.parity = 2;
  // reading a bit field
  printf("%d\n", a.parity);
  return 0;
}
