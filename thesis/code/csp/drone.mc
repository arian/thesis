// RTOS things
#define PERIPHERALS_RS232_DATA 0
#define PERIHPERALS_RS232_ISR 0
uint8 PERIPHERALS[];
void set_interrupt(int, void *(void));

// Application declarations
typedef struct {
  uint8 rotor1, rotor2, rotor3, rotor4;
} RotorSettings;

typedef struct {
  uint8 yaw, roll, pitch, speed;
} SetPoints;

void set_rotor_speeds(RotorSettings *);
void calculate_rotor_speeds(RotorSettings *, SetPoints *);

// a buffered channel, non-blocking writes
buffered chan<uint8> rs232_channel;

void rs232_isr() {
  uint8 c = PERIPHERALS[PERIPHERALS_RS232_DATA];
  rs232_channel ! c;
}

process receive_command(
    chan<uint8> c,
    chan<SetPoints> setpoint_channel @
    int8 current_command) {
  int8 value;
  static SetPoints setpoint;
  c ? value;
  switch (current_command) {
    case 0: setpoint.yaw = value; break;
    case 1: setpoint.roll = value; break;
    case 2: setpoint.pitch = value; break;
    case 3: setpoint.speed = value; break;
  }
  if (current_command == -1) {
    receive_command(value);
  } else {
    setpoint_channel ! setpoint;
    receive_command(-1);
  }
}

process controller(
    chan<RotorSettings> settings_channel,
    chan<SetPoints> setpoints_channel) {
  RotorSettings settings;
  SetPoints setpoints;
  setpoints_channel ? setpoints;
  calculate_rotor_speeds(&settings, &setpoints);
  settings_channel ! settings;
  controller();
}

process rotors(chan<RotorSettings> settings_channel) {
  RotorSettings settings;
  settings_channel ? settings;
  set_rotor_speeds(&settings);
  rotors();
}


int main() {
  chan<SetPoints> setpoints;
  chan<RotorSettings> rotors;
  set_interrupt(PERIHPERALS_RS232_ISR, &rs232_isr);
  par {
    receive_command(rs232_channel, setpoints @ -1);
    controller(rotors, setpoints);
    rotors(rotors);
  }
  return 0;
}
