buffered[2] chan<uint8> rs232_channel;

void rs232_isr() {
  /* may have received > 1 char before before ISR
     is serviced */
  while (PERIPHERALS[RS232_CHAR]) {
    uint8 c = PERIPHERALS[RS232_DATA];
    rs232_channel ! c;
  }
}

process receive_command(
    chan<uint8> c,
    chan<SetPoints> setpoint_channel @
    uint8 standby) {
  uint8 current_command, value;
  static SetPoints setpoint;
  c ? current_command;
  c ? value;
  if (current_command == STANDBY_CMD) {
    setpoints_reset(&setpoint);
    standby = value;
  } else if (standby == STANDBY_DISABLED) {
    switch (current_command) {
      case 0: setpoint.yaw = value; break;
      case 1: setpoint.roll = value; break;
      case 2: setpoint.pitch = value; break;
      case 3: setpoint.speed = value; break;
    }
  }
  setpoint_channel ! setpoint;
  receive_command(standby);
}
