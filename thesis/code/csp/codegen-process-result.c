#include <csp.h>
#include <process.h>
typedef struct  {
  signed int a, b;
} f_ProcessArgs;
void f (Process *process) {
  f_ProcessArgs *args = process->args;
  signed int a = args->a;
  signed int b = args->b;
  signed int x;
  { x = 0; }
  __csp_process_recurse: {
    signed int y = a + b;
    b = 31;
    goto __csp_process_recurse;
  }
}
