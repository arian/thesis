// A process of the name 'Producer', with an input channel 'c'
process Producer(chan<int> c) {
  // writes a number to the channel
  c ! 31;
}

// A process of the name 'Consumer', with an input channel 'c'
process Consumer(chan<int> c) {
  int num;
  // Read a value from the channel 'c', synchronize with the write
  c ? num;
  printf("%d\n", num);
}

int main() {
  // Declare a channel for integers
  chan<int> c;
  // Invoke the processes in parallel
  // Pass the channel into the processes
  par {
    Producer(c);
    Consumer(c);
  }
  // If both processes have terminated, continue the program
  return 0;
}
