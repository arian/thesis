typedef struct {
  uint8 rotor1, rotor2, rotor3, rotor4;
} RotorSettings;

typedef struct {
  uint8 yaw, roll, pitch, speed;
} SetPoints;

process controller(
    chan<SetPoints> setpoints_channel,
    chan<RotorSettings> settings_channel) {
  RotorSettings settings;
  SetPoints setpoints;
  setpoints_channel ? setpoints;
  calculate_rotor_speeds(&settings, &setpoints);
  settings_channel ! settings;
  controller();
}

process rotors(chan<RotorSettings> settings_channel) {
  RotorSettings settings;
  settings_channel ? settings;
  set_rotor_speeds(&settings);
  rotors();
}
