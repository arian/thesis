void g () {
  Channel *a = NULL;
  {
    Channel __a_csp_channel;
    if (a == NULL) {
      a = &__a_csp_channel;
      CSP_chanInit(a,
          CSP_ONE2ONE_CHANNEL, 0);
    }
    Process f_proc_1;
    f_ProcessArgs f_proc_1_args;
    f_proc_1_args.a = a;
    f_proc_1_args.b = 2;
    f_proc_1.args = &f_proc_1_args;
    ProcInit(&f_proc_1, f, NULL, 0, 0);
    ProcPar(&f_proc_1, NULL);
    ProcInitClean(&f_proc_1);
    if (a == &__a_csp_channel) {
      CSP_chanClose(a);
      a = NULL;
    }
  }
}
