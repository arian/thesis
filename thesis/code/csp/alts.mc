process controller2(
    chan<int> yaw,
    chan<int> roll,
    chan<int> pitch,
    chan<int> speed,
    chan<RotorSettings> settings_channel) {
  static SetPoints setpoints;
  int x;
  RotorSettings settings;
  alts {
    case yaw ? x: setpoints.yaw = x;
    case roll ? x: setpoints.roll = x;
    case pitch ? x: setpoints.pitch = x;
    case speed ? x: setpoints.speed = x;
  }
  calculate_rotor_speeds(&settings, &setpoints);
  settings_channel ! settings;
  controller2();
}
