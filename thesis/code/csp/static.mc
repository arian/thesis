process f() {
  static int x;
  process_init: { x = 0; }
  x++;
  f();
}
