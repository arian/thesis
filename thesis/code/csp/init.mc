int main() {
  chan<SetPoints> setpoint;
  chan<RotorSettings> rotors;
  set_interrupt(RS232_ISR, &rs232_isr);
  par {
    receive_command(rs232_channel, setpoint @ STANDBY_ENABLED);
    controller(setpoint, rotors);
    rotors(rotors);
  }
  return 0;
}
