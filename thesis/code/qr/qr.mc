#include <sys/neutrino.h>

#define COM1 3

struct calibration {
  int32 accx;
  int32 accy;
  int32 accz;
  int32 velp;
  int32 velq;
  int32 velr;
  uint8 set;
};
typedef struct calibration *Calibration;

statemachine QRMode {
  init standby;

  struct calibration _cal;
  Calibration cal = &_cal;

  event calibrate();
  event goto_standby();
  event goto_manual();

  state standby {
    on calibrate -> calibrate;
    on [cal->set] goto_manual -> manual;
  }

  state calibrate {
    entry {
      do_calibration(cal);
    }
    on -> standby;
  }

  state manual {
    entry {

    }
    on goto_standby -> standby;
  }


}

buffered chan<int> tick;

const struct sigevent* handler(void* area, int id) {
  tick ! GetCom1Value();
  return NULL;
}

struct parsed_cmd {
  int cmd;
  int values[3];
};

statemachine CommandParser {
  init start;

  int done;
  int index;
  struct parsed_cmd;

  event receive_char(char c);

  state start {
    entry {
      done = 0;
    }
    on receive_char [ c == 0x01 ] -> change_mode;
    on receive_char [ c == 0x02 ] -> change_speed;
    on receive_char [ c == 0x03 ] -> change_roll;
    on receive_char [ c == 0x04 ] -> change_yaw;
    on receive_char [ c == 0x05 ] -> change_pitch;
    on receive_char [ c == 0x06 ] -> change_ryp;
    on reset -> start;
  }

  state change_mode {
    entry { cmd.cmd = CHANGE_MODE; }
    on receive_char [ c >= 0 && c < 4] / {
      set_cmd_value(&cmd, 0, c);
    } -> finish;
    on reset -> start;
  }

  state change_speed {
    entry { cmd.cmd = CHANGE_SPEED; }
    on receive_char [ c >= 0 && <= 0xFF ] / {
      cmd.value[0] = c;
    } -> finish;
    on reset -> start;
  }

  // receive roll, yaw, pitch in one command
  state change_ryp {
    entry {
      cmd.cmd = CHANGE_RYP;
      index = 0;
    }
    on receive_char [ index < 3 && c >= 0 && <= 0xFF ] / {
      cmd.value[index++] = c;
    } -> finish;
    on reset -> start;
  }

  state finish {
    // a command should finish with a special char 0xFF
    on receive_char [ c == 0xFF ] -> done;
    on reset -> start;
  }

  state done {
    entry {
      done = 1;
    }
    on reset -> start;
  }
}

process handle_chars(chan<char> cc, chan<struct parsed_cmd> commands) {
  static statemachine CommandParser parser;
  if (parser<|done) {
    commands ! parser<|cmd;
  } else {
    char c;
    cc ? c;
    parser|>receive_char(c);
  }
  handle_chars();
}

int main() {

  // Attach ISR vector
  int id = InterruptAttach(COM1, &handler, NULL, 0, 0);

  statemachine QRMode mode;

  // Disconnect the ISR handler
  InterruptDetach(id);

  return 0;
}
