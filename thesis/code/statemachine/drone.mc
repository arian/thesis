statemachine Drone {
  init off;

  uint8 calibrated = 0;
  uint32 speed = 0;

  event onoff();
  event calibrate();
  event fly(uint32 input_speed);
  event land();

  state off {
    entry { calibrated = 0; }
    on onoff -> on;
  }
  state on {
    on onoff -> off;
    on calibrate -> calibrate;
    on fly [calibrated == 1] / speed = input_speed; -> flying;
  }
  state calibrate {
    entry { calibrated = 1; }
    on -> on;
  }
  state flying {
    on fly [calibrated == 1] / speed = input_speed; -> flying;
    on land -> on;
  }
}
