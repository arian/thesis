#include <assert.h>
void f() {
  statemachine Drone drone;

  drone<|onoff();

  // can't go to flying if it's not calibrated
  drone<|fly(10);
  assert(drone|>speed == 0);

  // calibrate, directly go to the on state again
  drone<|calibrate();
  assert(drone|>calibrated == 1);

  // now we can go to flying
  drone<|fly(20);
  assert(drone|>speed == 20);

  drone<|land();

  drone<|onoff();
}
