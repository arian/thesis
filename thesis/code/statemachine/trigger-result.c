Drone drone;
Drone_EventParams drone_event_args;
Drone_init(&drone);

(
  drone_event_args.input_speed = 20,
  Drone_exec(&drone, 2, &drone_event_args)
);
