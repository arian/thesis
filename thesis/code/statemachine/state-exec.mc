state on {
  on onoff -> off;
  on fly
    [calibrated == 1]
    / speed = input_speed;
    -> flying;
}
