#include <stdio.h>
// defines mc_fopen()
#include <mc_err_stdio.h>

void print_file() {
  char c;
  attempt {
    FILE *fp ?= mc_fopen(__FILE__, "r");
    while ((c = getc(fp)) != EOF) {
      printf("%c", c);
    }
  } fail {
    printf("could not print the file\n");
  }
}

int main() {
  print_file();
  return 0;
}
