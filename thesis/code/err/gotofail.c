#include <stdio.h>

void print_file() {
  char c;
  FILE *fp = fopen(__FILE__, "r");

  // does it return NULL, 0, -1 or something else?
  // not enforced to handle it at all.
  if (fp == NULL) {
    goto fail;
  }

  while ((c = getc(fp)) != EOF) {
    printf("%c", c);
  }

  // easy to forget 'return', so the error handling
  // code is not executed
  return;

fail:
  printf("could not print the file\n");
}

int main() {
  print_file();
  return 0;
}
