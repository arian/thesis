MaybeError<FILE*> mc_fopen(char *filename, char *modes) {
  FILE *fp = fopen(filename, modes);
  if (fp != NULL) {
    return fp;
  }
  return Error();
}
