char attempt_1_e;
{ MError_f __maybe_a;
  if (!f(&__maybe_a)) {
    attempt_1_e = __maybe_a.error;
    goto attempt_fail_1;
  }
  int a = __maybe_a.value;
  MError_g __maybe_b;
  if (!g(&__maybe_b)) {
    goto attempt_fail__;
  }
  int b = __maybe_b.value;
  goto attempt_finally;
}
attempt_fail_1: {
  { printf("f failed: %d\n",
      attempt_1_e); }
  goto attempt_finally; }
attempt_fail__: {
  { printf("g failed\n"); }
  goto attempt_finally; }
attempt_finally: {}
