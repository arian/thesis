void process_input(Process *proc) {
  int c1, c2;
  c1 = getchar();
  if (c1 == -1)
    goto fail;
  c2 = getchar();
  if (c2 == -1)
    goto fail;
  process_keys(proc, c1, c2);
  /* easy to forget 'return' to prevent the
     error handling code to always run */
  return;
fail:
  printf("could not read from the buffer\n");
}
