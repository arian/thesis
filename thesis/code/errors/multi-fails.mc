enum full_error {full};
enum empty_error {empty};
MaybeError<void,enum full_error> push(int32 x) {
  /* implementation */
}
MaybeError<int32,enum empty_error> pop() {
  /* implementation */
}
void f() {
  attempt {
    void p ?= push(1);
    int32 last ?= pop();
  } fail (enum full_error e) {
    printf("The stack is full\n");
  } fail (enum empty_error e) {
    printf("The stack is empty\n");
  }
}
