/* read char from rx fifo, return -1 if no char available */
int getchar() {
  int c;
  if (optr == iptr)
    return -1;
  c = fifo[optr++];
  if (optr > FIFOSIZE)
    optr = 0;
  return c;
}
