typedef union  {
  int value;
  char error;
} MError_f;
unsigned char f(
    MError_f *ret,
    int x) {
  if (x == 1)
    return (ret->error = 'E', 0);
  return (ret->value = 0, 1);
}
