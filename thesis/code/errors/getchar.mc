/* read char from rx fifo, return Error if no char available */
MaybeError<int> getchar() {
  int c;
  if (optr == iptr)
    return Error();
  c = fifo[optr++];
  if (optr > FIFOSIZE)
    optr = 0;
  return c;
}
