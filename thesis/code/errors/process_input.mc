void process_input(Process *proc) {
  attempt {
    int c1 ?= getchar();
    int c2 ?= getchar();
    process_keys(proc, c1, c2);
  } fail {
    printf("could not read from the buffer");
  }
}
