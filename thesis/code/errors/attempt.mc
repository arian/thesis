attempt {
  int a ?= f();
  int b ?= g();
} fail (char e) {
  printf("f failed: %c\n");
} fail {
  printf("g failed\n");
}
