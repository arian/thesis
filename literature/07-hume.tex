\section{Hume: A Domain Specific Language for Real-Time Embedded Systems}

Hume \cite{hammond2003hume} is a Domain Specific Language for Real-Time
Embedded Systems. Hume provides a number of high level features.

There are a number of essential desirable properties:

\begin{itemize}
  \item \emph{determinacy} The system should execute in equivalently under
    identical environment constraints.
  \item \emph{bounded time/space} The recourse costs should be statically
    bounded, so ensuring \emph{hard real-time} and \emph{real-space}
    constraints to be met.
  \item \emph{asynchronicity} The system should be able to respond to inputs
    as they are received.
  \item \emph{concurrency} The language must allow the construction of systems
    as communicating units of independent computation.
  \item {correctness} The language must allow a high degree of confidence that
    constructed systems meet their formal requirements.
\end{itemize}

The goal with the Hume design is to allow high level abstractions while still
maintaining these properties.

Process abstractions (\emph{boxes}) specify an asynchronous and stateless
mapping of inputs to outputs.

Boxes are wired explicitly into a static network, using \emph{wires} to link
\emph{boxes} and \emph{devices}.

A box is an abstraction of a finite state machine.

Boxes are connected with wires. A wire proves a mapping between an output link
to an input link.

The Box execution cycle is:

\begin{enumerate}
  \item check input availability for all inputs;
  \item match inputs against rules in turn;
  \item consume all inputs;
  \item bind variables to input values and evaluate the RHS of the selected
    rule;
  \item write output to the corresponding wires
\end{enumerate}

In the Hume model, there is a one-to-one correspondence between input and
output wires. An output wire can be connected to an input wire of the same box,
essentially creating an explicit representation of state.

The two primary coordination constructs that are used to introduce asynchronous
coordination are to \emph{ignore} certain inputs/outputs and to introduce
\emph{fair matching}.

Each box is implemented on a tread. Wires comprise a pair of a value and a
validity flag to ensure correct locking between input and output threads.
Using a built-in (round-robin) scheduler the threads are scheduled.

The Hume language is layered. On top of the boxes and wires, a MetaProgramming
layer exists to simplify the construction of boxes and wiring.

Exceptions must be handled by the surrounding layer directly. It is not
possible for errors to bubble up the stack-trace, to ensure tight bounds.

A precise model can statically calculate the costs of a program, using the
language declarations.

